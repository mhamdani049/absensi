export class ApiResponse {
    status: number;
    message: string;
    result: any;
    data: any;
    metadata: any;
}
