import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Formatter {
  constructor() {}

  public pad(str: string, max: number): String {
    str = str.toString();
    return str.length < max ? this.pad('0' + str, max) : str;
  }
}
