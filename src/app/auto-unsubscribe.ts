export function AutoUnsubscribe( blackList = [] ) {
    return function ( constructor ) {
        const original = constructor.prototype.ngOnDestroy;
        constructor.prototype.ngOnDestroy = function () {
            // for global with name variable.
            for ( let prop in this ) {
              const property = this[prop];
              if ( !blackList.includes(prop) ) if ( property && ( typeof property.unsubscribe === "function" ) ) property.unsubscribe();
            }

            // for table service.
            if (this.tableSvc)
              for ( let prop in this.tableSvc ) {
                const property = this.tableSvc[prop];
                if ( property && ( typeof property.unsubscribe === "function" ) ) property.unsubscribe();
              }

            original && typeof original === 'function' && original.apply(this, arguments);
        };
    }
}
