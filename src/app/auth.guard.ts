import { Injectable } from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from './services/auth.service';
import { PermissionService } from './services/permission.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
 /* canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }*/

  constructor(private router: Router, private authService: AuthService, private permissionService: PermissionService) { }
  // CanActivate method override
  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (this.authService.isLoggedIn()) {
          // logged in so return true
          const routerPermission = route.data.permissions;
          const getPermissionRouter = routerPermission ? routerPermission.only : [];
          if (routerPermission !== undefined && getPermissionRouter.length > 0) {
              console.log('==================== start check permission ====================');
              let setPermission = '';
              await getPermissionRouter.forEach(function (permission) {
                console.log('1/2. Name Permission: ', permission);
                setPermission = permission;
              });
              const cp = await this.permissionService.checkPermission(setPermission);
              console.log('2/2. Status Permission: ', cp);
              console.log('==================== end check permission ====================');
              if (cp) { return true; } else {
                console.log('Permission denied redirect to page /permissionDenied');
                this.router.navigate(['/permissionDenied']);
                return false;
              }

          } else {
            return true;
          }
      }
      // If not login user the redirect to login page
      this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url }});
      return false;
  }
}
