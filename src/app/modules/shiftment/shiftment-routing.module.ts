import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShiftmentListComponent } from './shiftment-list/shiftment-list.component';
import { ShiftmentFormComponent } from './shiftment-form/shiftment-form.component';

import { AuthGuard } from '../../auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/shiftment/list',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'list',
        component: ShiftmentListComponent,
        canActivate: [ AuthGuard ],
        data: {
          permissions: {
            only: ['canViewMasterUserList'],
          }
        }
      }
    ]
  },
  {
    path: 'form',
    component: ShiftmentFormComponent,
    canActivate: [ AuthGuard ],
    data: {
      permissions: {
        only: ['canCreateMasterUserList'],
      }
    }
  },
  {
    path: 'form/:id',
    component: ShiftmentFormComponent,
    canActivate: [ AuthGuard ],
    data: {
      permissions: {
        only: ['canEditMasterUserList'],
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShiftmentRoutingModule { }
