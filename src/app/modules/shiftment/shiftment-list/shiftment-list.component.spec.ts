import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftmentListComponent } from './shiftment-list.component';

describe('ShiftmentListComponent', () => {
  let component: ShiftmentListComponent;
  let fixture: ComponentFixture<ShiftmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftmentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
