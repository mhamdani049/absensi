export const settings = {
  primary: 'id',
  columns: {
    id: {
      title: 'ID',
      type: 'hidden',
      width: 80,
      table: false
    },
    code: {
      title: 'Code',
      type: 'string',
      search: true,
      sort: true
    },
    name: {
      title: 'Name',
      type: 'string',
      search: true,
      sort: true
    },
    startHour: {
      title: 'Start Hour',
      type: 'time',
      search: false,
      sort: true
    },
    endHour: {
      title: 'End Hour',
      type: 'time',
      sort: true,
      search: false
    },
    department: {
      title: 'Department',
      type: 'select',
      child: {
        label: 'name'
      },
      options: [],
      sort: false,
      search: true
    },
    createdAt: {
      title: 'CreatedAt',
      type: 'datetime',
      width: 160,
      search: true,
      form: false,
      sort: true
    }
  },
  tools: {
    update: {
      icon: 'fa fa-edit',
      class: 'btn-info',
      label: '',
      click: '/shiftment/form/[id]',
      permission: 'canEditMasterUserList'
    },
    delete: {
      icon: 'fa fa-trash',
      class: 'btn-danger',
      label: '',
      permission: 'canDeleteMasterUserList'
    }
  },
  fieldTypeDate: ['createdAt'],
  fieldCollect: ['department'],
  filterAllExcept: ['all', 'active'],
  filterAllCollect: [
    {
      fieldCollect: 'department',
      fieldInParent: 'department',
      model: 'department',
      fieldWhere: 'name',
      fieldKey: 'id'
    }
  ]
};
