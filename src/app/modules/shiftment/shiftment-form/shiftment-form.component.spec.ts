import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftmentFormComponent } from './shiftment-form.component';

describe('ShiftmentFormComponent', () => {
  let component: ShiftmentFormComponent;
  let fixture: ComponentFixture<ShiftmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
