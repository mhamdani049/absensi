import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShiftmentRoutingModule } from './shiftment-routing.module';
import { ShiftmentListComponent } from './shiftment-list/shiftment-list.component';

import { SharedModule } from '@component/shared.module';
import { ShiftmentFormComponent } from './shiftment-form/shiftment-form.component';

@NgModule({
  declarations: [ShiftmentListComponent, ShiftmentFormComponent],
  imports: [
    CommonModule,
    ShiftmentRoutingModule,
    SharedModule
  ]
})
export class ShiftmentModule { }
