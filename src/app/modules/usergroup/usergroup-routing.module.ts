import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsergroupListComponent } from './usergroup-list/usergroup-list.component';
import { UsergroupFormComponent } from './usergroup-form/usergroup-form.component'
import { UsergroupPrivilegeComponent } from './usergroup-privilege/usergroup-privilege.component'

const routes: Routes = [
  { path: '', redirectTo: '/usergroup', pathMatch: 'full' },
  { path: '', component: UsergroupListComponent },
  { path: 'form', component: UsergroupFormComponent },
  { path: 'form/:id', component: UsergroupFormComponent },
  { path: 'privilege/:id', component: UsergroupPrivilegeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsergroupRoutingModule { }
