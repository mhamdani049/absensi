import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { DataService } from '@services/data.service';
import { settings } from '../usergroup-setting';
import swal from 'sweetalert2';

@Component({
  selector: 'app-usergroup-form',
  templateUrl: './usergroup-form.component.html',
  styleUrls: ['./usergroup-form.component.css']
})
export class UsergroupFormComponent implements OnInit {

  user = {};
  id = null;
  title = '';
  userLoaded = false;
  settings = settings;
  model = '/usergroup';

  constructor(
    private route: ActivatedRoute,
    private api: ApiService,
    private dataSvc: DataService,
  ) {
    this.id = this.route.snapshot.params['id'];
    this.title = this.id === undefined ? 'Add Form' : 'Edit Form';
   }

  ngOnInit() {
    this.route.snapshot.paramMap.get('id') ? this.getUsers() : this.userLoaded = true;
  }

  getUsers() {
    const params = {};
    const where = {};
    where[settings.primary] = this.route.snapshot.paramMap.get('id');
    params['where'] = JSON.stringify(where);
    console.log('params', params);
    this.dataSvc.get(this.model, params).subscribe(
      res => {
        console.log(res);
        this.userLoaded = true;
        if (res.status === 'success') { this.user = res.data[0]; }
      },
      error => {
        this.userLoaded = true;
        console.log('error', error);
      }
    );
  }

  onSubmit(dataSubmit): void {
    console.log('dataSubmit', dataSubmit);
    if (dataSubmit.id) { this.excUpdate(dataSubmit); } else { this.excCreate(dataSubmit); }
  }

  excCreate(dataSubmit) {
    delete dataSubmit.id;
    this.dataSvc.post(this.model, dataSubmit).subscribe(
      res => {
        console.log(res);
        this.alertSuccess('Success created');
      },
      error => {
        // this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  excUpdate(dataSubmit) {
    this.dataSvc.put(this.model + '/' + dataSubmit.id, dataSubmit).subscribe(
      res => {
        console.log(res);
        if (res.status === 'success') {
          this.alertSuccess('Data Updated');
        }
      },
      error => {
        // this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  alertSuccess(message) {
    swal.fire({
      title: 'Success',
      text: message,
      timer: 2000,
      showConfirmButton: false,
      type: 'success'
    });
  }

  alertError(message) {
    swal.fire({
      title: 'Oops',
      text: message || 'Somethign wrong',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-info',
      type: 'error'
    });
  }

}
