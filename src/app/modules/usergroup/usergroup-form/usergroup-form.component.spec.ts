import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsergroupFormComponent } from './usergroup-form.component';

describe('UsergroupFormComponent', () => {
  let component: UsergroupFormComponent;
  let fixture: ComponentFixture<UsergroupFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsergroupFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsergroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
