import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '@services/data.service';
import swal from 'sweetalert2';
import * as _ from 'lodash';

@Component({
  selector: 'app-usergroup-privilege',
  templateUrl: './usergroup-privilege.component.html',
  styleUrls: ['./usergroup-privilege.component.css']
})
export class UsergroupPrivilegeComponent implements OnInit {

  treemenu;
  tree;
  changePriv = { update: {}, remove: {}, add: {} };
  modulePriv = {};
  redeToSave = false;

  constructor(
    private route: ActivatedRoute,
    private dataSvc: DataService
  ) { }

  ngOnInit() {
    this.getTreeMenu();
  }

  getTreeMenu() {
    const params = {};
    this.dataSvc.get('/usergroup/treemenu?isUser=0&idUserGroup=' + this.route.snapshot.paramMap.get('id'), params).subscribe(
      res => {
        console.log('res /usergroup/treemenu', res);
        // this.treemenu = res.data;
        this.treemenu = this.initTree(res.data, 1, []);
      },
      error => {
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  savePriv(dataSubmit) {
    this.dataSvc.post('/usergroup/privilege', dataSubmit).subscribe(
      res => {
        this.getTreeMenu();
        this.alertSuccess('Privilege Updated');
      },
      error => {
        // this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  initTree(dt, lv, r) {
    const level = lv || 1;
    const res = r || [];
    if (dt.length) {
      dt.forEach(function (n) {
        // tslint:disable-next-line:max-line-length
        this.changePriv.update[n.id] = n;
        this.modulePriv[n.id] = n.permission;
        const nData = _.clone(n);
        nData.level = level;
        nData.itemsLength = nData.submenu.length;
        delete nData.submenu;

        res.push(nData);
        // console.log(res);

        if (n.submenu.length) {
          // console.log(n.submenu.length);
          this.initTree(n.submenu, level + 1, res);
        }
      }.bind(this));
      return res;
    } else { return; }
  }

  clickUpdate(dt) {
    this.initUpdatePrev(dt);
    const child = this.getChildTree(dt.id, []);
    if (child.length) {
      child.forEach(function (n) {
        this.modulePriv[n.id] = this.modulePriv[dt.id];
        this.initUpdatePrev(n);
      }.bind(this));
    }

    const parent = this.getParentTree(dt, []);
    if (parent.length) {
      parent.forEach(function (n) {
        if (this.modulePriv[dt.id]) {
          this.modulePriv[n.id] = this.modulePriv[dt.id];
          this.initUpdatePrev(n);
        }
      }.bind(this));
    }
  }

  initUpdatePrev(dt) {
    // console.log('>>>', dt);
    if (dt.idAuth && !this.modulePriv[dt.id]) {
      this.changePriv.remove[dt.idAuth] = { idAuth: dt.idAuth, title: dt.title };
    }
    if (dt.idAuth && this.modulePriv[dt.id]) {
      delete this.changePriv.remove[dt.idAuth];
    }
    if (!dt.idAuth && this.modulePriv[dt.id]) {
      if (typeof this.changePriv.update[dt.id] !== 'undefined') {
        this.changePriv.update[dt.id].groupId = this.route.snapshot.paramMap.get('id');
        this.changePriv.update[dt.id].title = dt.title;
        this.changePriv.update[dt.id].path = dt.path;
        this.changePriv.update[dt.id].id = dt.id;
      } else {
        // tslint:disable-next-line:max-line-length
        this.changePriv.update[dt.id] = { 'groupId': this.route.snapshot.paramMap.get('id'), 'title': dt.title, 'path': dt.path, 'id': dt.id };
      }
    }
    if (!dt.idAuth && !this.modulePriv[dt.id]) {
      // delete this.changePriv.update[dt.id];
      // tslint:disable-next-line:max-line-length
      this.changePriv.update[dt.id] = { bitView: false, bitPrint: false, bitExcel: false, bitCreate: false, bitEdit: false, bitDelete: false };
    }

    this.redeToSave = _.size(this.changePriv.add) || _.size(this.changePriv.remove) ? true : false;
  }

  getChildTree(id, r) {
    const res = r || [];
    r = _.filter(this.treemenu, { parent: id });
    if (r.length) {
      r.forEach(function (n) {
        console.log('child', n);
        res.push(n);
        this.getChildTree(n.id, res);
      }.bind(this));
    }
    return res;
  }

  getParentTree(dt, r) {
    const res = r || [];
    r = _.filter(this.treemenu, { id: dt.parent });
    if (r.length) {
      r.forEach(function (n) {
        // console.log('parent', n);
        res.push(n);
        this.getParentTree(n, res);
      }.bind(this));
    }
    return res;
  }

  alertSuccess(message) {
    swal.fire({
      title: 'Success',
      text: message,
      timer: 2000,
      showConfirmButton: false,
      type: 'success'
    });
  }

  alertError(message) {
    swal.fire({
      title: 'Oops',
      text: message || 'Somethign wrong',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-info',
      type: 'error'
    });
  }

}
