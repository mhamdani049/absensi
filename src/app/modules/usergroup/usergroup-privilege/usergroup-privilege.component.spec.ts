import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsergroupPrivilegeComponent } from './usergroup-privilege.component';

describe('UsergroupPrivilegeComponent', () => {
  let component: UsergroupPrivilegeComponent;
  let fixture: ComponentFixture<UsergroupPrivilegeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsergroupPrivilegeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsergroupPrivilegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
