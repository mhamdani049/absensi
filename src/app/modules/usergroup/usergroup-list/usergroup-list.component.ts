import { Component, OnInit } from '@angular/core';
import { TableService } from '../../../services/table.service';
import { DataService } from '@services/data.service';
import { settings } from '../usergroup-setting';
import { AutoUnsubscribe } from '@app/auto-unsubscribe';
import swal from 'sweetalert2';

@AutoUnsubscribe([]) // ini untuk handle -> HTTP Inspector Jika "Cancel".
@Component({
  selector: 'app-usergroup-list',
  templateUrl: './usergroup-list.component.html',
  styleUrls: ['./usergroup-list.component.css']
})
export class UsergroupListComponent implements OnInit {
  settings = settings;
  model = '/usergroup';

  constructor(
    public tableSvc: TableService,
    private dataSvc: DataService
  ) {}

  ngOnInit() {
    this.initTable();
  }

  initTable() {
    this.tableSvc.where = {};
    this.tableSvc.page = 1;
    const tblOpt = {
      endPoint: this.model,
      limit: 10,
      sort: 'id desc',
      collect: '',
      criteriaMapping: {
        VCH_GRPNAME: 'contains'
      }
    };
    this.tableSvc.initialize(tblOpt);
    this.tableSvc.requestData();
  }

  deleteRow(id) {
    swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.excUpdate({id, deleted: 1});
      }
    });
  }

  excUpdate(dataSubmit) {
    this.dataSvc.put(this.model + '/' + dataSubmit.id, dataSubmit).subscribe(
      res => {
        console.log(res);
        if (res.status === 'success') {
          this.alertSuccess('Data Deleted');
          this.tableSvc.requestData();
        }
      },
      error => {
        // this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  alertSuccess(message) {
    swal.fire({
      title: 'Success',
      text: message,
      timer: 2000,
      showConfirmButton: false,
      type: 'success'
    });
  }

  alertError(message) {
    swal.fire({
      title: 'Oops',
      text: message || 'Somethign wrong',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-info',
      type: 'error'
    });
  }
}
