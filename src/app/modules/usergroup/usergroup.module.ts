import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule  } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPermissionsModule } from 'ngx-permissions';

import { UsergroupRoutingModule } from './usergroup-routing.module';
import { UsergroupListComponent } from './usergroup-list/usergroup-list.component';
import { UsergroupFormComponent } from './usergroup-form/usergroup-form.component';

import { SharedModule } from '@component/shared.module';
import { UsergroupPrivilegeComponent } from './usergroup-privilege/usergroup-privilege.component';

@NgModule({
  declarations: [
    UsergroupListComponent,
    UsergroupFormComponent,
    UsergroupPrivilegeComponent
  ],
  imports: [
    CommonModule,
    UsergroupRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    NgxPermissionsModule
  ]
})
export class UsergroupModule { }
