export const settings = {
  primary: 'id',
  columns: {
    id: {
      title: 'ID',
      type: 'hidden',
      width: 80,
      table: false
    },
    groupName: {
      title: 'Name',
      type: 'string',
      search: true,
      sort: true
    },
    createdAt: {
      title: 'CreatedAt',
      type: 'datetime',
      width: 180,
      search: true,
      form: false,
      sort: true
    }
  },
  tools: {
    rules: {
      icon: 'fa fa-lock',
      class: 'btn-info',
      label: ' ',
      click: '/usergroup/privilege/[id]',
      permission: 'canEditMasterGroupList'
    },
    update: {
      icon: 'fa fa-edit',
      class: 'btn-info',
      label: ' ',
      click: '/usergroup/form/[id]',
      permission: 'canEditMasterGroupList'
    },
    delete: {
      icon: 'fa fa-trash',
      class: 'btn-danger',
      // click action delete nya di handle otomatis
      label: ' ',
      permission: 'canDeleteMasterGroupList'
    }
  },
  // sseting for filter & filter all
  fieldTypeDate: ['createdAt'],
  fieldCollect: [],
  filterAllExcept: ['all'],
  filterAllCollect: []
};
