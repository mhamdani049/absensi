import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '@services/data.service';
import swal from 'sweetalert2';
import * as  _ from 'lodash';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent implements OnInit {

  menus: any;
  displayTree = {};

  constructor(
    private route: ActivatedRoute,
    private dataSvc: DataService
  ) { }

  ngOnInit() {
    this.getMenu();
  }

  getMenu() {
    const params = {};
    this.dataSvc.get('/menu', params).subscribe(
      res => {
        console.log('res /menu', res);
        this.menus = this.treemenu(res.data);
      },
      error => {
        console.log('error', error);
      }
    );
  }

  treemenu(menus) {
    menus.forEach(e => {
      this.displayTree[e.parent] = typeof this.displayTree[e.parent] === 'undefined' ? true : this.displayTree[e.parent];
      this.displayTree[e.id] = typeof this.displayTree[e.id] === 'undefined' ? true : this.displayTree[e.id];
      const childMenu = <any>_.filter(menus, { parent: e.id });
      e.childMenu = childMenu.length ? childMenu.sort(function (a, b) { return a.sorting - b.sorting; }) : [];
    });
    const menutree = <any>_.filter(menus, { parent: 0 }); menutree.sort(function (a, b) { return a.sorting - b.sorting; });
    const menutreetolist = this.tolist(menutree, 1, []);
    return menutreetolist;
  }


  tolist(dt, lv, res) {
    if (dt.length) {
      dt.forEach(function (n) {
        const newData = _.clone(n);
        newData.level = lv;
        newData.childMenuLength = newData.childMenu.length;
        delete newData.childMenu;
        res.push(newData);

        if (n.childMenu.length) {
          this.tolist(n.childMenu, lv + 1, res);
        }
      }.bind(this));
      return res;
    } else { return; }
  }

  toggleTree(dt) {
    this.displayTree[dt.id] = !this.displayTree[dt.id];
    const child = this.getChildTree(dt.id, []);
    if (child.length) {
      child.forEach(function (n) {
        this.displayTree[n.id] = this.displayTree[dt.id];
      }.bind(this));
    }
  }

  getChildTree(id, _r) {
    const res = _r || [];
    const r = _.filter(this.menus, { parent: id });
    if (r.length) {
      r.forEach(function (n) {
        res.push(n);
        this.getChildTree(n.id, res);
      }.bind(this));
    }
    return res;
  }

  setSort(idx, action) {
    let current = this.menus[idx];
    let replace = this.menus[action === 'up' ? idx - 1 : idx + 1];
    if (replace.parent !== current.parent) {
      const listTmp = _.filter(this.menus, { level: current.level, id: Number });
      const idx2 = _.findIndex(listTmp, { id: current.id });
      const current2 = <any>listTmp[idx2];
      const replace2 = <any>listTmp[action === 'up' ? idx2 - 1 : idx2 + 1];

      if (replace2 && replace2.parent === current2.parent) {
        current = current2;
        replace = replace2;
      }
    }


    if (replace && replace.parent === current.parent) {
      const currentSortingOld = _.clone(current.sorting);
      current.sorting = replace.sorting;
      replace.sorting = currentSortingOld;

      this.menus = this.treemenu(this.menus);
    }
  }

  removeData(menu) {
    swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.dataSvc.put('/menu/' + menu.id, { deleted: 1 }).subscribe(
          res => {
            console.log(res);
            this.alertSuccess('Data deleted');
            this.getMenu();
          },
          error => {
            this.alertError(error.error.message);
            console.log('error', error);
          }
        );
      }
    });
  }

  alertSuccess(message) {
    swal.fire({
      title: 'Success',
      text: message,
      timer: 2000,
      showConfirmButton: false,
      type: 'success'
    });
  }

  alertError(message) {
    swal.fire({
      title: 'Oops',
      text: message || 'Somethign wrong',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-info',
      type: 'error'
    });
  }

}
