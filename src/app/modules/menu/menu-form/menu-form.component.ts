import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { DataService } from '../../../services/data.service';
import { settings } from '../menu-setting';
import swal from 'sweetalert2';
import * as _ from 'lodash';

@Component({
  selector: 'app-menu-form',
  templateUrl: './menu-form.component.html',
  styleUrls: ['./menu-form.component.css']
})
export class MenuFormComponent implements OnInit {

  menu = {};
  userLoaded = false;
  settings = settings;

  constructor(
    private route: ActivatedRoute,
    private api: ApiService,
    private dataSvc: DataService,
  ) { }

  ngOnInit() {
    this.getParent();
    this.route.snapshot.paramMap.get('id') ? this.getDetail() : this.userLoaded = true;
  }

  getDetail() {
    const params = {};
    const where = {};
    where[settings.primary] = this.route.snapshot.paramMap.get('id');
    params['where'] = JSON.stringify(where);
    console.log('params', params);
    this.dataSvc.get('/menu', params).subscribe(
      res => {
        console.log(res);
        this.userLoaded = true;
        if (res.status === 'success') { this.menu = res.data[0]; }
      },
      error => {
        this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  getParent(): void {
    this.dataSvc.get('/menu', { where: JSON.stringify({ deleted: '0' }) })
      .subscribe(res => {
        const opt = [];
        const tree = this.treemenu(res.data);
        tree.forEach(e => { opt.push({ val: e.id, label: e.title }); });
        this.settings.columns.parent.options = opt;
      },
        error => {
          this.alertError(error.error.message);
          console.log('error', error);
        });
  }

  onSubmit(dataSubmit): void {
    if (!dataSubmit.parent) { delete dataSubmit.parent; }
    if (!dataSubmit.icon) { dataSubmit.icon = ''; }
    console.log('dataSubmit', dataSubmit);
    if (dataSubmit.id) { this.excUpdate(dataSubmit); } else { this.excCreate(dataSubmit); }
  }

  excCreate(dataSubmit) {
    delete dataSubmit.id;
    this.dataSvc.post('/menu/', dataSubmit).subscribe(
      res => {
        console.log(res);
        this.alertSuccess('Success created');
      },
      error => {
        // this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  excUpdate(dataSubmit) {
    this.dataSvc.put('/menu/' + dataSubmit.id, dataSubmit).subscribe(
      res => {
        console.log(res);
        if (res.status === 'success') {
          this.alertSuccess('Data Updated');
        }
      },
      error => {
        // this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  treemenu(menus) {
    menus.forEach(e => {
      const childMenu = <any>_.filter(menus, { parent: e.id });
      e.childMenu = childMenu.length
        ? childMenu.sort(function (a, b) { return a.sorting - b.sorting; })
        : [];
    });
    const menutree = _.filter(menus, { parent: 0 });
    const menutreetolist = this.tolist(menutree, 1, []);
    console.log(menutreetolist);

    return menutreetolist;
  }

  tolist(dt, lv, res) {
    if (dt.length) {
      dt.forEach(function (n) {
        const newData = _.clone(n);
        newData.level = lv;
        newData.childMenuLength = newData.childMenu.length;
        newData.title = (lv === 3 ? '---- ' : (lv === 2 ? '-- ' : '')) + newData.title;
        delete newData.childMenu;
        res.push(newData);

        if (n.childMenu.length) {
          this.tolist(n.childMenu, lv + 1, res);
        }
      }.bind(this));
      return res;
    } else { return; }
  }

  alertSuccess(message) {
    swal.fire({
      title: 'Success',
      text: message,
      timer: 2000,
      showConfirmButton: false,
      type: 'success'
    });
  }

  alertError(message) {
    swal.fire({
      title: 'Oops',
      text: message || 'Somethign wrong',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-info',
      type: 'error'
    });
  }

}
