export const settings = {
    primary: 'id',
    columns: {
      id: {
        title: 'ID',
        type: 'hidden',
        width: 80
      },
      parent : {
        title: 'Parent',
        type: 'select',
        required: false,
        child: {
          label: 'title'
        },
        options: []
      },
      title: {
        title: 'Name',
        type: 'string',
        search: true
      },
      alias: {
        title: 'Alias',
        type: 'string',
        search: true
      },
      path: {
        title: 'Path',
        type: 'string',
        required: false,
        search: true
      },
      icon: {
        title: 'Icon',
        type: 'string',
        required: false,
        search: true
      },
      sorting: {
        title: 'Sorting',
        type: 'string',
        search: true
      },
      active: {
        title: 'Status',
        type: 'select',
        width: 100,
        options: [
          {val: '1', label: 'Active'},
          {val: '0', label: 'Inactive'},
        ]
      },
      createdAt: {
        title: 'CreatedAt',
        type: 'date',
        width: 180,
        search: true,
        form: false
      }
    },
    tools: {
      update: {
        icon: 'fa fa-edit',
        class: 'btn-info',
        label: ' ',
        click: '/menu/form/[id]',
        permission: ''
      },
      delete: {
        icon: 'fa fa-trash',
        class: 'btn-danger',
        // click action delete nya di handle otomatis  
        label: ' ',
        permission: ''
      }
    }
  };
