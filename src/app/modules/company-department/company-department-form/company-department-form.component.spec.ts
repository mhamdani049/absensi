import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyDepartmentFormComponent } from './company-department-form.component';
import {describe, expect} from 'jasmine';

describe('CompanyDepartmentFormComponent', () => {
  let component: CompanyDepartmentFormComponent;
  let fixture: ComponentFixture<CompanyDepartmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyDepartmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyDepartmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
