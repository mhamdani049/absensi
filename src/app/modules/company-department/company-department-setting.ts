export const settings = {
  primary: 'id',
  columns: {
    id: {
      title: 'ID',
      type: 'hidden',
      width: 80,
      table: false
    },
    name: {
      title: 'Name',
      type: 'string',
      search: true,
      sort: true
    },
    company: {
      title: 'Company',
      type: 'select',
      child: {
        label: 'name'
      },
      options: [],
      sort: false,
      search: true
    },
    code: {
      title: 'Code',
      type: 'string',
      search: true,
      sort: true
    },
    createdAt: {
      title: 'CreatedAt',
      type: 'datetime',
      width: 160,
      search: true,
      form: false,
      sort: true
    }
  },
  tools: {
    update: {
      icon: 'fa fa-edit',
      class: 'btn-info',
      label: '',
      click: '/companyDepartment/form/[id]',
      permission: 'canEditMasterUserList'
    },
    delete: {
      icon: 'fa fa-trash',
      class: 'btn-danger',
      label: '',
      // click action delete nya di handle auto
      permission: 'canDeleteMasterUserList'
    }
  },
  // setting for filter & filter all
  fieldTypeDate: ['createdAt'],
  fieldCollect: ['company'],
  filterAllExcept: ['all', 'active'],
  filterAllCollect: [
    {
      fieldCollect: 'company',
      fieldInParent: 'company',
      model: 'company',
      fieldWhere: 'name',
      fieldKey: 'id'
    }
  ]
};
