import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyDepartmentListComponent } from './company-department-list.component';
import {describe, expect} from 'jasmine';

describe('CompanyDepartmentListComponent', () => {
  let component: CompanyDepartmentListComponent;
  let fixture: ComponentFixture<CompanyDepartmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyDepartmentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyDepartmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
