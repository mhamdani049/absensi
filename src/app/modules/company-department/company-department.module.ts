import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyDepartmentRoutingModule } from './company-department-routing.module';
import { CompanyDepartmentListComponent } from './company-department-list/company-department-list.component';

import { SharedModule } from '@component/shared.module';
import { CompanyDepartmentFormComponent } from './company-department-form/company-department-form.component';

@NgModule({
  declarations: [CompanyDepartmentListComponent, CompanyDepartmentFormComponent],
  imports: [
    CommonModule,
    CompanyDepartmentRoutingModule,
    SharedModule
  ]
})
export class CompanyDepartmentModule { }
