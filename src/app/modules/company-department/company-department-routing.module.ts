import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyDepartmentListComponent } from './company-department-list/company-department-list.component';
import { CompanyDepartmentFormComponent } from './company-department-form/company-department-form.component';

import { AuthGuard } from '../../auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/companyDepartment/list',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'list',
        component: CompanyDepartmentListComponent,
        canActivate: [ AuthGuard ],
        data: {
          permissions: {
            only: ['canViewMasterUserList'],
          }
        }
      }
    ]
  },
  {
    path: 'form',
    component: CompanyDepartmentFormComponent,
    canActivate: [ AuthGuard ],
    data: {
      permissions: {
        only: ['canCreateMasterUserList'],
      }
    }
  },
  {
    path: 'form/:id',
    component: CompanyDepartmentFormComponent,
    canActivate: [ AuthGuard ],
    data: {
      permissions: {
        only: ['canEditMasterUserList'],
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyDepartmentRoutingModule { }
