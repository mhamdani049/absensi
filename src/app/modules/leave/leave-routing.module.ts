import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaveFormComponent } from './leave-form/leave-form.component';
import { LeaveListComponent } from './leave-list/leave-list.component';

import { AuthGuard } from '../../auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/leave/list',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'list',
        component: LeaveListComponent,
        canActivate: [AuthGuard],
        data: {
          // permissions: {
          //   only: ['canViewMasterUserList']
          // }
        }
      }
    ]
  },
  {
    path: 'form',
    component: LeaveFormComponent,
    canActivate: [AuthGuard],
    data: {
      // permissions: {
      //   only: ['canCreateMasterUserList']
      // }
    }
  },
  {
    path: 'form/:id',
    component: LeaveFormComponent,
    canActivate: [AuthGuard],
    data: {
      // permissions: {
      //   only: ['canEditMasterUserList']
      // }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeaveRoutingModule { }
