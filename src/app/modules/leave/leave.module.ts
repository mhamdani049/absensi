import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaveRoutingModule } from './leave-routing.module';
import { LeaveFormComponent } from './leave-form/leave-form.component';
import { LeaveListComponent } from './leave-list/leave-list.component';
import { SharedModule } from '@component/shared.module';

@NgModule({
  declarations: [LeaveFormComponent, LeaveListComponent],
  imports: [
    CommonModule,
    LeaveRoutingModule,
    SharedModule
  ]
})
export class LeaveModule { }
