export const settings = {
  primary: 'id',
  columns: {
    id: {
      title: 'ID',
      type: 'hidden',
      width: 80,
      table: false
    },
    user: {
      title: 'Nama',
      type: 'text',
      sort: true,
      search: true,
      child: {
        label: "fullname"
      },
    },
    description: {
      title: 'Keterangan',
      type: 'text',
      sort: true,
      search: true
    },
    status: {
      title: 'Status',
      type: 'text',
      sort: true,
      search: true
    },
    createdAt: {
      title: 'CreatedAt',
      type: 'datetime',
      width: 160,
      search: true,
      form: false,
      sort: true
    },

  },
  tools: {
    dateoff: {
      icon: 'fa fa-date',
      class: 'btn-info',
      label: '',
      click: '/leave/dateoff/[id]',
      permission: 'canEditMasterUserList'
    },
    update: {
      icon: 'fa fa-edit',
      class: 'btn-info',
      label: '',
      click: '/leave/form/[id]',
      permission: 'canEditMasterUserList'
    },
    delete: {
      icon: 'fa fa-trash',
      class: 'btn-danger',
      label: '',
      permission: 'canDeleteMasterUserList'
    }
  },
  fieldTypeDate: ['createdAt', 'dateOff'],
  fieldCollect: [],
  filterAllExcept: ['all'],
  filterAllCollect: []
};
