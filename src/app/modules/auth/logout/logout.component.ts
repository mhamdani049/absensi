import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';

import { AuthService } from '@app/services/auth.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  returnUrl: string;

  constructor(
    private ngxps: NgxPermissionsService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService) { }

  ngOnInit() {
    this.ngxps.flushPermissions(); // remove gnx-permissions
    this.authService.logout();
    // get return url from route parameters or default to '/'
    this.router.navigate(['/auth/login']);
  }

}
