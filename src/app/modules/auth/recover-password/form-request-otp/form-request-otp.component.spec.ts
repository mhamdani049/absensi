import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRequestOtpComponent } from './form-request-otp.component';

describe('FormRequestOtpComponent', () => {
  let component: FormRequestOtpComponent;
  let fixture: ComponentFixture<FormRequestOtpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRequestOtpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRequestOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
