import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormVerificationOtpComponent} from '@app/modules/auth/recover-password/form-verification-otp/form-verification-otp.component';
import { ApiService } from './../../../../services/api.service'
import swal from 'sweetalert2'

@Component({
  selector: 'app-form-request-otp',
  templateUrl: './form-request-otp.component.html',
  styleUrls: ['./form-request-otp.component.css']
})
export class FormRequestOtpComponent implements OnInit {

  recoverPasswordForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  changePasswordWith = 'email';

  constructor(
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.onChangeRecoverPasswordWith(this.changePasswordWith);
  }

  onChangeRecoverPasswordWith(val) {
    console.log("onChangeRecoverPasswordWith", val);
    this.changePasswordWith = val;

    if (val === 'email') {
      this.recoverPasswordForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        phone: ['']
      })
    } else {
      const regexPattern = /\-?\d*\.?\d{1,2}/;
      this.recoverPasswordForm = this.formBuilder.group({
        email: [''],
        phone: ['', [Validators.required, Validators.pattern(regexPattern)]]
      })
    }
  }

  get f() {
    return this.recoverPasswordForm.controls;
  }

  onSubmit() {
    console.log("onSubmit loaded", this.recoverPasswordForm.controls);
    this.submitted = true;
    this.recoverPasswordForm.value.via = this.changePasswordWith;

    if (this.changePasswordWith === 'email') this.recoverPasswordForm.value.phone = '';
    else if (this.changePasswordWith === 'phone') this.recoverPasswordForm.value.email = '';

    if (this.recoverPasswordForm.invalid) return;

    console.log("onSubmit this.f", this.recoverPasswordForm.value);
    this.loading = true;
    this.apiService.post('/user/req-change-password', this.recoverPasswordForm.value).subscribe((res) => {
      console.log("onSubmit res:", res);
      this.loading = false;
      this.openModal(this.recoverPasswordForm.value)
    }, (err) => {
      console.log("onSubmit err:", err);
      this.loading = false;
      swal.fire({
        title: 'Oops',
        text: err.message || 'Somethign wrong',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-info',
        type: 'error'
      })
    });
  }

  openModal(datas) {
    console.log("openModal loaded");
    const modalRef = this.modalService.open(FormVerificationOtpComponent, { backdrop: 'static' });
    modalRef.componentInstance.data = datas;

    modalRef.result.then((res) => {
      console.log("openModal res:", res)
    }).catch((err) => {
      console.log("openModal err:", err)
    })
  }



}
