import {Component, Injectable, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '@services/api.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-form-verification-otp',
  templateUrl: './form-verification-otp.component.html',
  styleUrls: ['./form-verification-otp.component.css']
})
@Injectable({
  providedIn: "root"
})
export class FormVerificationOtpComponent implements OnInit {
  @Input() data: any;

  form: FormGroup;
  otpValid = false;
  submitted = false;
  otp: string;
  loading = false;

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) {

  }

  ngOnInit() {
    console.log("this.data", this.data);
    this.form = this.formBuilder.group({
      otp_one: ['', Validators.required],
      otp_two: ['', Validators.required],
      otp_three: ['', Validators.required],
      otp_four: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.minLength(6)]]
    });

    setTimeout(() => {
      this.timerResendVerification()
    }, 300);
  }

  get f() {
    return this.form.controls;
  }

  closeModal(msg) {
    this.activeModal.close(msg)
  }

  onClickVerification() {
    console.log("onClickVerification loaded");
    console.log("onClickVerification loaded f:", this.form.value, this.form.controls);

    this.submitted = true;
    if (this.form.invalid) return;
    this.loading = true;
    this.otp = this.form.value.otp_one + '' + this.form.value.otp_two + '' + this.form.value.otp_three + '' + this.form.value.otp_four;
    this.apiService.post('/user/change-password-byotp', {otp: this.otp, newPassword: this.form.value.newPassword}).subscribe((res) => {
      console.log("onSubmit res:", res);
      this.loading = false;
      swal.fire({
        title: 'Success',
        text: `${res.data} \n Please login again with your new password.`,
        timer: 2000,
        showConfirmButton: false,
        type: 'success'
      });
      this.closeModal('Close Modal');
      this.router.navigate(['/auth/login']);
    }, (err) => {
      console.log("onSubmit err:", err);
      this.loading = false;
      swal.fire({
        title: 'Oops',
        text: err.message || 'Somethign wrong',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-info',
        type: 'error'
      })
    });
  }

  onClickResend() {
    console.log("onClickResend loaded");
    this.loading = true;
    this.apiService.post('/user/req-change-password', this.data).subscribe((res) => {
      console.log("onSubmit res:", res);
      this.loading = false;
      swal.fire({
        title: 'Success',
        text: res.data,
        timer: 2000,
        showConfirmButton: false,
        type: 'success'
      });
      this.timerResendVerification()
    }, (err) => {
      console.log("onSubmit err:", err);
      this.loading = false;
      swal.fire({
        title: 'Oops',
        text: err.message || 'Somethign wrong',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-info',
        type: 'error'
      })
    });
  }

  timerResendVerification() {
    console.log("timerResendVerification loaded");

    var spn = document.getElementById("count");
    var btn = document.getElementById("btn-verification");

    let count = 20;
    let timer = null;

    btn.setAttribute('disabled', 'disabled');
    (function countDown() {
      spn.textContent = count.toString();
      if (count !== 0) {
        timer = setTimeout(countDown, 1000);
        count--;
      } else {
        btn.removeAttribute('disabled');
      }
    }())
  }

}
