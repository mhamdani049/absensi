import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormVerificationOtpComponent } from './form-verification-otp.component';

describe('FormVerificationOtpComponent', () => {
  let component: FormVerificationOtpComponent;
  let fixture: ComponentFixture<FormVerificationOtpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormVerificationOtpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormVerificationOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
