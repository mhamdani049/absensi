import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { FormRequestOtpComponent } from './recover-password/form-request-otp/form-request-otp.component';
import { FormVerificationOtpComponent } from './recover-password/form-verification-otp/form-verification-otp.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    LoginComponent,
    LogoutComponent,
    FormRequestOtpComponent,
    FormVerificationOtpComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    NgbModule
  ],
  entryComponents: [
    FormVerificationOtpComponent
  ]
})
export class AuthModule { }
