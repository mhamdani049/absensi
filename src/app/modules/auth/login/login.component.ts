import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { first } from 'rxjs/operators';
import { NgxPermissionsService } from 'ngx-permissions';


import { PermissionService } from '@app/services/permission.service';

import { AuthService } from '@app/services/auth.service';
declare var gapi: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public auth2: any;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private ngxps: NgxPermissionsService,
    private permissionService: PermissionService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService) {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl(this.returnUrl);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      identifier: ['', Validators.required],
      password: ['', Validators.required]
    });
    // reset login status
    // this.authService.logout();
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authService.login(this.f.identifier.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          // this.permission.initPermission(); -- backup @comment by yusup at 29 Jan 2018
          this.permissionService.load().then((permissions) => {
            console.log("login success now init permissions ==> ", permissions);
            this.ngxps.loadPermissions(permissions);

            // this.router.navigateByUrl(this.returnUrl, {skipLocationChange: true});
            this.router.navigate([this.returnUrl]);
          }).then((err) => {
            console.log('something went wrong:', err);
          });
        },
        error => {
          console.log('error', error);
          this.error = error.message;
          this.loading = false;
        });
  }

  public googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '418185088340-6c2lghjtbce9gitsqmpd88dokgtpqcqo.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });
      this.attachSignin(document.getElementById('googleBtn'));
    });
  }
  public attachSignin(element) {
    this.auth2.attachClickHandler(element, {},
      (googleUser) => {

        // let profile = googleUser.getBasicProfile();
        //console.log('Token || ' + googleUser.getAuthResponse().id_token);
        //console.log('ID: ' + profile.getId());
        //console.log('Name: ' + profile.getName());
        //console.log('Image URL: ' + profile.getImageUrl());
        //console.log('Email: ' + profile.getEmail());
        //YOUR CODE HERE
        this.authService.loginWithGoogle(googleUser.getAuthResponse().id_token)
          .pipe(first())
          .subscribe(
            data => {
              // this.permission.initPermission(); -- backup @comment by yusup at 29 Jan 2018
              this.permissionService.load().then((permissions) => {
                console.log("login success now init permissions ==> ", permissions);
                this.ngxps.loadPermissions(permissions);
                // this.router.navigateByUrl(this.returnUrl, {skipLocationChange: true});
                this.router.navigate([this.returnUrl]);
              }).then((err) => {
                console.log('something went wrong:', err);
              });
            },
            error => {
              console.log('error', error);
              this.error = error.message;
              this.loading = false;
            });

      }, (error) => {
        console.log(JSON.stringify(error, undefined, 2));
        // alert(JSON.stringify(error, undefined, 2));
      });
  }

  ngAfterViewInit() {
    this.googleInit();
  }
}
