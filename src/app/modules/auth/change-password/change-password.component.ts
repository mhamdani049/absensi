import { Component, OnInit } from '@angular/core'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { environment } from '@environments/environment'
import swal from 'sweetalert2'
import { catchError, first, tap } from 'rxjs/operators'
import { Observable, of } from 'rxjs'

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  constructor(private http: HttpClient) {}
  apiUrl = environment.apiUrl
  f = {
    oldPassword: '',
    newPassword: '',
    confirmNewPassword: ''
  }
  submitting = false
  ngOnInit() {}

  onSubmit() {
    let params = { ...this.f }
    delete params['confirmNewPassword']
    this.submitting = true
    this.http
      .post<any>(`${this.apiUrl}/user/change-my-password`, params)
      // .pipe(catchError(this.handleError('onSubmit')))
      .subscribe(
        res => {
          this.submitting = false
          console.log('/change-my-password', res)
          swal.fire({
            title: 'Success',
            text: 'Your password has changed',
            timer: 2000,
            showConfirmButton: false,
            type: 'success'
          })
          this.f = {
            oldPassword: '',
            newPassword: '',
            confirmNewPassword: ''
          }
        },
        (error: HttpErrorResponse) => {
          this.submitting = false
          console.log('err /change-my-password', error)
          swal.fire({
            title: 'Oops',
            text: error.error.message || 'Somethign wrong',
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-info',
            type: 'error'
          })
        }
      )
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead
      console.log(`${operation} failed: ${error.message}`)

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
