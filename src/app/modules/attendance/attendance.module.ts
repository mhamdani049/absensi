import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@component/shared.module';

import { AttendanceRoutingModule } from './attendance-routing.module';
import { LogComponent } from './log/log.component';

@NgModule({
  declarations: [LogComponent],
  imports: [
    CommonModule,
    AttendanceRoutingModule,
    SharedModule
  ]
})
export class AttendanceModule { }
