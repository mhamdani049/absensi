import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {
  dates: string[];
  employees;
  date = new Date();
  constructor() { }

  ngOnInit() {
    this.getEmployees();
    this.dates= this.getDaysInMonth(this.date.getMonth(),this.date.getFullYear())
    console.log('date',this.getDaysInMonth(this.date.getMonth(),this.date.getFullYear()))
  }

  getDaysInMonth(month, year) {
    const date = new Date(year, month, 1);
    const days = [];
    while (date.getMonth() === month) {
       days.push(new Date(date));
       date.setDate(date.getDate() + 1);
    }
    return days;
}
getEmployees() {
  this.employees = [{
    'id': 1,
    'name': 'Clovis'
  }, {
    'id': 2,
    'name': 'Terrence'
  }, {
    'id': 3,
    'name': 'Ora'
  }, {
    'id': 4,
    'name': 'Lisabeth'
  }, {
    'id': 5,
    'name': 'Hill'
  }, {
    'id': 6,
    'name': 'Florella'
  }, {
    'id': 7,
    'name': 'Anders'
  }, {
    'id': 8,
    'name': 'Nicolais'
  }, {
    'id': 9,
    'name': 'Mickey'
  }, {
    'id': 10,
    'name': 'Yehudi'
  }, {
    'id': 11,
    'name': 'Petr'
  }, {
    'id': 12,
    'name': 'Layne'
  }];
}
  range(n) {
    return new Array(n);
  };
}
