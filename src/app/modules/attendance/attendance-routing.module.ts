import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogComponent } from './log/log.component';

import { AuthGuard } from '../../auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/attendance',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'log',
        component: LogComponent,
      //  canActivate: [ AuthGuard ], // AuthGuard return false alert, NgxPermissionsGuard return false redirect to page permissionDenied
        data: {
          permissions: {
           // only: ['canViewAttendanceList'],
            // redirectTo: 'permissionDenied' // for canActivate: [ NgxPermissionsGuard ]
          }
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AttendanceRoutingModule { }
