import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { DataService } from '../../../services/data.service';
import { settings } from '../company-setting';
import swal from 'sweetalert2';

@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.css']
})
export class CompanyFormComponent implements OnInit {

  public title = '';
  public id = null;
  settings = settings;
  companyLoaded = false;
  company = {};
  model = '/company';

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private permissionService: NgxPermissionsService,
      private dataSvc: DataService,
    ) {
    this.id = this.route.snapshot.params['id'];
    this.title = this.id === undefined ? 'Add Form' : 'Edit Form';
  }

  ngOnInit() {
    this.route.snapshot.paramMap.get('id') ? this.getcompanys() : this.companyLoaded = true;
  }

  getcompanys() {
    const params = {};
    const where = {};
    where[settings.primary] = this.route.snapshot.paramMap.get('id');
    params['where'] = JSON.stringify(where);
    this.dataSvc.get(this.model, params).subscribe(
      res => {
        console.log(res);
        this.companyLoaded = true;
        if (res.status === 'success') { this.company = res.data[0]; }
      },
      error => {
        this.companyLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  onSubmit(dataSubmit): void {
    console.log('dataSubmit', dataSubmit);
    if (dataSubmit.id) { this.excUpdate(dataSubmit); } else { this.excCreate(dataSubmit); }
  }

  excCreate(dataSubmit) {
    delete dataSubmit.id;
    this.dataSvc.post(this.model, dataSubmit).subscribe(
      res => {
        console.log(res);
        this.alertSuccess('Success created');
      },
      error => {
        // this.companyLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  excUpdate(dataSubmit) {
    if (!dataSubmit.password) { delete dataSubmit.password; }
    this.dataSvc.put(this.model + '/' + dataSubmit.id, dataSubmit).subscribe(
      res => {
        console.log(res);
        if (res.status === 'success') {
          this.alertSuccess('Data Updated');
        }
      },
      error => {
        // this.companyLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  alertSuccess(message) {
    swal.fire({
      title: 'Success',
      text: message,
      timer: 2000,
      showConfirmButton: false,
      type: 'success'
    });
  }

  alertError(message) {
    swal.fire({
      title: 'Oops',
      text: message || 'Something wrong',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-info',
      type: 'error'
    });
  }

}

