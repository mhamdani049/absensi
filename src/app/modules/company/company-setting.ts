export const settings = {
  primary: 'id',
  columns: {
    id: {
      title: 'ID',
      type: 'hidden',
      width: 80,
      table: false
    },
    name: {
      title: 'Name',
      type: 'string',
      search: true,
      sort: true
    },
    email: {
      title: 'Email',
      type: 'string',
      search: true,
      sort: true
    },
    code: {
      title: 'Code',
      type: 'string',
      search: true,
      sort: true
    },
    createdAt: {
      title: 'CreatedAt',
      type: 'datetime',
      width: 160,
      search: true,
      form: false,
      sort: true
    }
  },
  tools: {
    update: {
      icon: 'fa fa-edit',
      class: 'btn-info',
      label: '',
      click: '/company/form/[id]',
      permission: 'canEditMasterCompanyList'
    },
    delete: {
      icon: 'fa fa-trash',
      class: 'btn-danger',
      label: '',
      // click action delete nya di handle auto
      permission: 'canDeleteMasterCompanyList'
    }
  },
  // setting for filter & filter all
  fieldTypeDate: ['createdAt'],
  fieldCollect: ['userGroup'],
  filterAllExcept: ['all', 'active'],
  filterAllCollect: [
    {
      fieldCollect: 'userGroup',
      fieldInParent: 'userGroup',
      model: 'usergroup',
      fieldWhere: 'groupName',
      fieldKey: 'id'
    }
  ]
};
