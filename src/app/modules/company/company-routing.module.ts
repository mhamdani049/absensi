import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyFormComponent } from './company-form/company-form.component';

import { AuthGuard } from '../../auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/company/list',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'list',
        component: CompanyListComponent,
        canActivate: [ AuthGuard ], // AuthGuard return false alert, NgxPermissionsGuard return false redirect to page permissionDenied
        data: {
          permissions: {
            only: ['canViewMasterCompanyList'],
            // redirectTo: 'permissionDenied' // for canActivate: [ NgxPermissionsGuard ]
          }
        }
      }
    ]
  },
  {
    path: 'form',
    component: CompanyFormComponent,
    canActivate: [ AuthGuard ], // AuthGuard return false alert, NgxPermissionsGuard return false redirect to page permissionDenied
    data: {
      permissions: {
        only: ['canCreateMasterCompanyList'],
        // redirectTo: 'permissionDenied' // for canActivate: [ NgxPermissionsGuard ]
      }
    }
  },
  {
    path: 'form/:id',
    component: CompanyFormComponent,
    canActivate: [ AuthGuard ], // AuthGuard return false alert, NgxPermissionsGuard return false redirect to page permissionDenied
    data: {
      permissions: {
        only: ['canEditMasterCompanyList'],
        // redirectTo: 'permissionDenied' // for canActivate: [ NgxPermissionsGuard ]
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
