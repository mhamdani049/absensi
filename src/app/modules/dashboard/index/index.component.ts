import { Component, OnInit } from '@angular/core';
import {ApiResponse} from '@app/api-response';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AutoUnsubscribe} from '@app/auto-unsubscribe';

// @AutoUnsubscribe([]) // ini untuk handle -> HTTP Inspector Jika "Cancel".
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  reqGetPendingRequest$;

  constructor( private http: HttpClient ) {}

  ngOnInit() {
    //this.getPendingRequest();
  }

  getPendingRequest() {
   this.reqGetPendingRequest$ = this.http.get<ApiResponse>(`http://10.234.99.220:8081/demo/1.post`, { headers: new HttpHeaders({ timeout: `${20000}` }) }).subscribe((response) => {  })
  }

}
