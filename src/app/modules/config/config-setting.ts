export const settings = {
  primary: 'id',
  columns: {
    id: {
      title: 'ID',
      type: 'hidden',
      width: 80,
      table: false
    },
    groupConfig: {
      title: 'Group name',
      type: 'string',
      search: true,
      sort: true
    },
    variable: {
      title: 'Variable name',
      type: 'string',
      search: true,
      sort: true
    },
    valueConfig: {
      title: 'Value',
      type: 'string',
      search: true,
      sort: true
    },
    valueType: {
      title: 'Type',
      type: 'string',
      search: true,
      sort: true
    },
    createdAt: {
      title: 'CreatedAt',
      type: 'datetime',
      width: 180,
      search: true,
      form: false,
      sort: true
    }
  },
  tools: {
    update: {
      icon: 'fa fa-edit',
      class: 'btn-info',
      label: ' ',
      click: '/config/form/[id]',
      permission: 'canEditMasterGroupList'
    },
    delete: {
      icon: 'fa fa-trash',
      class: 'btn-danger',
      // click action delete nya di handle otomatis
      label: ' ',
      permission: 'canEditMasterGroupList'
    }
  }
};
