import { Component, OnInit } from '@angular/core';
import { DataService } from '@services/data.service';
import { TableService } from '@app/services/table.service';
import { settings } from '../config-setting';
import { AutoUnsubscribe } from '@app/auto-unsubscribe';
import swal from 'sweetalert2';

@AutoUnsubscribe([])
@Component({
  selector: 'app-config-list',
  templateUrl: './config-list.component.html',
  styleUrls: ['./config-list.component.css']
})
export class ConfigListComponent implements OnInit {
  settings = settings;
  model = '/config';
  constructor(
    private dataSvc: DataService,
    public tableSvc: TableService
  ) { }

  ngOnInit() {
    this.initTable();
  }
  initTable() {
    this.tableSvc.where = {};
    this.tableSvc.page = 1;
    const tblOpt = {
      endPoint: this.model,
      limit: 10,
      sort: 'id desc',
      collect: '',
      criteriaMapping: {
        groupName: 'contains'
      }
    };
    this.tableSvc.initialize(tblOpt);
    this.tableSvc.requestData();
  }

  deleteRow(id) {
    swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.excUpdate({id, deleted: 1});
      }
    });
  }

  excUpdate(dataSubmit) {
    this.dataSvc.put(this.model + '/' + dataSubmit.id, dataSubmit).subscribe(
      res => {
        console.log(res);
        if (res.status === 'success') {
          this.alertSuccess('Data Deleted');
          this.tableSvc.requestData();
        }
      },
      error => {
        // this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  alertSuccess(message) {
    swal.fire({
      title: 'Success',
      text: message,
      timer: 2000,
      showConfirmButton: false,
      type: 'success'
    });
  }

  alertError(message) {
    swal.fire({
      title: 'Oops',
      text: message || 'Somethign wrong',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-info',
      type: 'error'
    });
  }
}
