import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigListComponent } from './config-list/config-list.component'
import { ConfigFormComponent } from './config-form/config-form.component'

const routes: Routes = [
  { path: '', redirectTo: '/config', pathMatch: 'full' },
  { path: '', component: ConfigListComponent },
  { path: 'form', component: ConfigFormComponent },
  { path: 'form/:id', component: ConfigFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigRoutingModule { }
