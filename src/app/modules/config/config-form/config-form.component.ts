import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '@app/services/api.service';
import { DataService } from '@app/services/data.service';
import { settings } from '../config-setting';
import swal from 'sweetalert2';

@Component({
  selector: 'app-config-form',
  templateUrl: './config-form.component.html',
  styleUrls: ['./config-form.component.css']
})
export class ConfigFormComponent implements OnInit {

  config = {};
  dataLoaded = false;
  settings = settings;
  model = '/config'

  constructor(
    private route: ActivatedRoute,
    private dataSvc: DataService,
  ) { }

  ngOnInit() {
    this.route.snapshot.paramMap.get('id') ? this.findOne() : this.dataLoaded = true;
  }

  findOne() {
    this.dataSvc.get(this.model + '/' + this.route.snapshot.paramMap.get('id')).subscribe(
      res => {
        console.log(res);
        this.dataLoaded = true;
        if (res.status === 'success') { this.config = res.data; }
      },
      error => {
        this.dataLoaded = true;
        console.log('error', error);
      }
    );
  }

  onSubmit(dataSubmit): void {
    console.log('dataSubmit', dataSubmit);
    if (dataSubmit.id) { this.excUpdate(dataSubmit); } else { this.excCreate(dataSubmit); }
  }

  excCreate(dataSubmit) {
    delete dataSubmit.id;
    this.dataSvc.post(this.model, dataSubmit).subscribe(
      res => {
        console.log(res);
        this.alertSuccess('Success created');
      },
      error => {
        // this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  excUpdate(dataSubmit) {
    this.dataSvc.put(this.model + '/' + dataSubmit.id, dataSubmit).subscribe(
      res => {
        console.log(res);
        if (res.status === 'success') {
          this.alertSuccess('Data Updated');
        }
      },
      error => {
        // this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }
  alertSuccess(message) {
    swal.fire({
      title: 'Success',
      text: message,
      timer: 2000,
      showConfirmButton: false,
      type: 'success'
    });
  }

  alertError(message) {
    swal.fire({
      title: 'Oops',
      text: message || 'Something wrong',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-info',
      type: 'error'
    });
  }

}
