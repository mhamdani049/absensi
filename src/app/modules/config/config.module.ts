import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPermissionsModule } from 'ngx-permissions';


import { ConfigListComponent } from './config-list/config-list.component';
import { SharedModule } from '@component/shared.module';
import { ConfigRoutingModule } from './config-routing.module';
import { ConfigFormComponent } from './config-form/config-form.component'

@NgModule({
  declarations: [ConfigListComponent, ConfigFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    NgxPermissionsModule,
    ConfigRoutingModule
  ]
})
export class ConfigModule { }
