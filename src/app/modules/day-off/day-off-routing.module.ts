import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DayOffListComponent } from './day-off-list/day-off-list.component';
import { DayOffFormComponent } from './day-off-form/day-off-form.component';

import { AuthGuard } from '../../auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dayoff/list',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'list',
        component: DayOffListComponent,
        canActivate: [ AuthGuard ],
        data: {
          // permissions: {
          //   only: ['canViewMasterUserList']
          // }
        }
      }
    ]
  },
  {
    path: 'form',
    component: DayOffFormComponent,
    canActivate: [ AuthGuard ],
    data: {
      // permissions: {
      //   only: ['canCreateMasterUserList']
      // }
    }
  },
  {
    path: 'form/:id',
    component: DayOffFormComponent,
    canActivate: [ AuthGuard ],
    data: {
      // permissions: {
      //   only: ['canEditMasterUserList']
      // }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DayOffRoutingModule { }
