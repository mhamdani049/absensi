export const settings = {
  primary: 'id',
  columns: {
    id: {
      title: 'ID',
      type: 'hidden',
      width: 80,
      table: false
    },
    dateOff: {
      title: 'Date',
      type: 'date',
      sort: true,
      search: true
    },
    notes: {
      title: 'Notes',
      type: 'string',
      search: true,
      sort: true
    },
    createdAt: {
      title: 'CreatedAt',
      type: 'datetime',
      width: 160,
      search: true,
      form: false,
      sort: true
    }
  },
  tools: {
    update: {
      icon: 'fa fa-edit',
      class: 'btn-info',
      label: '',
      click: '/dayOff/form/[id]',
      permission: 'canEditMasterUserList'
    },
    delete: {
      icon: 'fa fa-trash',
      class: 'btn-danger',
      label: '',
      permission: 'canDeleteMasterUserList'
    }
  },
  fieldTypeDate: ['createdAt', 'dateOff'],
  fieldCollect: [],
  filterAllExcept: ['all'],
  filterAllCollect: []
};
