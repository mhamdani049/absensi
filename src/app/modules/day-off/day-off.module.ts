import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DayOffRoutingModule } from './day-off-routing.module';
import { DayOffListComponent } from './day-off-list/day-off-list.component';

import { SharedModule } from '@component/shared.module';
import { DayOffFormComponent } from './day-off-form/day-off-form.component';

@NgModule({
  declarations: [DayOffListComponent, DayOffFormComponent],
  imports: [
    CommonModule,
    DayOffRoutingModule,
    SharedModule
  ]
})
export class DayOffModule { }
