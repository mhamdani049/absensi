import { Component, OnInit } from '@angular/core';
import { DataService } from '@services/data.service';
import { TableService } from '../../../services/table.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { settings } from '../day-off-setting';
import { AutoUnsubscribe } from '@app/auto-unsubscribe';
import swal from 'sweetalert2';

@AutoUnsubscribe([])
@Component({
  selector: 'app-day-off-list',
  templateUrl: './day-off-list.component.html',
  styleUrls: ['./day-off-list.component.css']
})
export class DayOffListComponent implements OnInit {

  settings = settings;
  model = '/dayOff';

  constructor(
    private dataSvc: DataService,
    public tableSvc: TableService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.initTable();
  }

  initTable() {
    this.tableSvc.where = {};
    this.tableSvc.page = 1;
    const tblOpt = {
      endPoint: this.model,
      limit: 5,
      sort: 'id desc',
      criteriaMapping: {
        notes: 'contains'
      }
    };

    this.tableSvc.initialize(tblOpt);
    this.tableSvc.requestData();
  }


  deleteRow(id) {
    swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.excUpdate({id, deleted: 1});
      }
    });
  }

  excUpdate(dataSubmit) {
    this.dataSvc.put(this.model + '/' + dataSubmit.id, dataSubmit).subscribe(
      res => {
        console.log(res);
        if (res.status === 'success') {
          this.alertSuccess('Data Deleted');
          this.tableSvc.requestData();
        }
      },
      error => {
        // this.userLoaded = true;
        this.alertError(error.error.message);
        console.log('error', error);
      }
    );
  }

  alertSuccess(message) {
    swal.fire({
      title: 'Success',
      text: message,
      timer: 2000,
      showConfirmButton: false,
      type: 'success'
    });
  }

  alertError(message) {
    swal.fire({
      title: 'Oops',
      text: message || 'Something wrong',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-info',
      type: 'error'
    });
  }

}
