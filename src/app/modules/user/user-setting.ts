export const settings = {
  primary: 'id',
  columns: {
    id: {
      title: 'ID',
      type: 'hidden',
      width: 80,
      table: false
    },
    fullname: {
      title: 'Name',
      type: 'string',
      search: true,
      sort: true
    },
    email: {
      title: 'Email',
      type: 'string',
      search: true,
      sort: true
    },
    username: {
      title: 'Username',
      type: 'string',
      search: true,
      sort: true
    },
    userGroup: {
      title: 'Group',
      type: 'select',
      child: {
        label: 'groupName'
      },
      options: [],
      sort: false,
      search: true
    },

    password: {
      title: 'Password',
      type: 'password',
      table: false,
      search: false,
      required: false,
      sort: false,
      textHelpEdit: 'leave blank if you don\'t want to change it'
    },
    active: {
      title: 'Status',
      type: 'select',
      width: 100,
      search: true,
      as: {
        '1': '<span class="badge badge-success">Active</span>',
        '0': '<span class="badge badge-danger">Non-Active</span>'
      },
      options: [{ val: '1', label: 'Active' }, { val: '0', label: 'Inactive' }],
      sort: true
    },
    createdAt: {
      title: 'CreatedAt',
      type: 'datetime',
      width: 160,
      search: true,
      form: false,
      sort: true
    }
  },
  tools: {
    update: {
      icon: 'fa fa-edit',
      class: 'btn-info',
      label: '',
      click: '/user/form/[id]',
      permission: 'canEditMasterUserList'
    },
    delete: {
      icon: 'fa fa-trash',
      class: 'btn-danger',
      label: '',
      // click action delete nya di handle auto
      permission: 'canDeleteMasterUserList'
    }
  },
  // setting for filter & filter all
  fieldTypeDate: ['createdAt'],
  fieldCollect: ['userGroup'],
  filterAllExcept: ['all', 'active'],
  filterAllCollect: [
    {
      fieldCollect: 'userGroup',
      fieldInParent: 'userGroup',
      model: 'usergroup',
      fieldWhere: 'groupName',
      fieldKey: 'id'
    }
  ]
};
