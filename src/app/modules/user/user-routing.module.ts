import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';

import { AuthGuard } from '../../auth.guard';

import { UserListComponent } from './user-list/user-list.component';
import { UserFormComponent } from './user-form/user-form.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/user/list',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'list',
        component: UserListComponent,
        canActivate: [ AuthGuard ], // AuthGuard return false alert, NgxPermissionsGuard return false redirect to page permissionDenied
        data: {
          permissions: {
            only: ['canViewMasterUserList'],
            // redirectTo: 'permissionDenied' // for canActivate: [ NgxPermissionsGuard ]
          }
        }
      },
      {
        path: 'form',
        component: UserFormComponent,
        canActivate: [ AuthGuard ], // AuthGuard return false alert, NgxPermissionsGuard return false redirect to page permissionDenied
        data: {
          permissions: {
            only: ['canCreateMasterUserList'],
            // redirectTo: 'permissionDenied' // for canActivate: [ NgxPermissionsGuard ]
          }
        }
      },
      {
        path: 'form/:id',
        component: UserFormComponent,
        canActivate: [ AuthGuard ], // AuthGuard return false alert, NgxPermissionsGuard return false redirect to page permissionDenied
        data: {
          permissions: {
            only: ['canEditMasterUserList'],
            // redirectTo: 'permissionDenied' // for canActivate: [ NgxPermissionsGuard ]
          }
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
