export const settings = {
  primary: 'id',
  columns: {
    id: {
      title: 'ID',
      type: 'hidden',
      width: 80,
      table: false
    },
    fullname: {
      title: 'User',
      type: 'string',
      search: true,
      sort: true
    },
    object: {
      title: 'Controller',
      type: 'string',
      sort: true,
      search: true
    },
    action: {
      title: 'Action',
      type: 'string',
      sort: true,
      search: true
    },
    method: {
      title: 'Method',
      type: 'string',
      sort: true,
      search: true
      // as: {
      //   'PUT': '<span class="badge badge-warning">Update</span>',
      //   'POST': '<span class="badge badge-info">Create / Update</span>',
      //   'DELETE': '<span class="badge badge-info">Delete</span>',
      // },
    },
    params: {
      title: 'Params',
      type: 'string',
      sort: false,
      search: true
    },
    createdAt: {
      title: 'Date',
      type: 'datetime',
      width: 160,
      search: true,
      form: false,
      sort: true
    }
  },
  tools: {
    //   update: {
    //     icon: 'fa fa-edit',
    //     class: 'btn-info',
    //     label: '',
    //     click: '/user/form/[id]',
    //     permission: 'canEditMasterUserList'
    //   },
    //   delete: {
    //     icon: 'fa fa-trash',
    //     class: 'btn-danger',
    //     label: '',
    //     permission: 'canDeleteMasterUserList'
    //   }
  },
  // setting for filter & filter all
  fieldTypeDate: ['createdAt'],
  fieldCollect: [],
  filterAllExcept: ['all'],
  filterAllCollect: [
    //   {
    //     fieldCollect: 'userGroup',
    //     fieldInParent: 'userGroup',
    //     model: 'usergroup',
    //     fieldWhere: 'groupName',
    //     fieldKey: 'id'
    //   }
  ]
};
