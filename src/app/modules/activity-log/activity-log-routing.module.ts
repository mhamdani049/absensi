import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AuthGuard } from '../../auth.guard'
import { ActivityLogListComponent } from './activity-log-list/activity-log-list.component'
const routes: Routes = [
  {
    path: '',
    redirectTo: '/activity-log/list',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'list',
        component: ActivityLogListComponent,
        canActivate: [AuthGuard],
        data: {
          permissions: {
            only: []
          }
        }
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityLogRoutingModule {}
