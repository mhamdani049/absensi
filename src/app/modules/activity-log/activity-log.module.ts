import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule } from '@angular/forms'
import { NgxPermissionsModule } from 'ngx-permissions'

import { SharedModule } from '@component/shared.module'
import { ActivityLogRoutingModule } from './activity-log-routing.module'
import { ActivityLogListComponent } from './activity-log-list/activity-log-list.component'

@NgModule({
  declarations: [ActivityLogListComponent],
  imports: [
    CommonModule,
    ActivityLogRoutingModule,
    NgbModule,
    FormsModule,
    NgxPermissionsModule,
    SharedModule
  ]
})
export class ActivityLogModule {}
