import { Component, OnInit } from '@angular/core'
import { TableService } from '../../../services/table.service'
import { settings } from '../activity-log-setting'
import { AutoUnsubscribe } from '@app/auto-unsubscribe'

@Component({
  selector: 'app-activity-log-list',
  templateUrl: './activity-log-list.component.html',
  styleUrls: ['./activity-log-list.component.css']
})
export class ActivityLogListComponent implements OnInit {
  settings = settings

  constructor(public tableSvc: TableService) {}

  ngOnInit() {
    this.initTable()
  }

  initTable() {
    this.tableSvc.where = {}
    this.tableSvc.page = 1
    const tblOpt = {
      endPoint: '/logs',
      limit: 10,
      sort: 'id desc',
      collect: '',
      criteriaMapping: {
        fullname: 'contains',
        object: 'contains',
        action: 'contains',
        method: 'contains',
        params: 'contains'
      }
    }
    this.tableSvc.initialize(tblOpt)
    this.tableSvc.requestData()
  }
}
