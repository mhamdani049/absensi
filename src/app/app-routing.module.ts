import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthLayoutComponent } from './layout/auth-layout/auth-layout.component';
import { ContentLayoutComponent } from './layout/content-layout/content-layout.component';
import { PageNotFoundComponent } from './layout/page-not-found/page-not-found.component';

import { AuthGuard } from './auth.guard';

import { PermissionDeniedComponent } from './layout/permission-denied/permission-denied.component';
import { ChangePasswordComponent } from './modules/auth/change-password/change-password.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: '',
    component: ContentLayoutComponent,
    canActivate: [AuthGuard], // Should be replaced with actual auth guard
    children: [
      {
        path: 'dashboard',
        loadChildren: './modules/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'user',
        loadChildren: './modules/user/user.module#UserModule'
      },
      {
        path: 'company',
        loadChildren: './modules/company/company.module#CompanyModule'
      },
      {
        path: 'companyDepartment',
        loadChildren: './modules/company-department/company-department.module#CompanyDepartmentModule'
      },
      {
        path: 'dayOff',
        loadChildren: './modules/day-off/day-off.module#DayOffModule'
      },
      {
        path: 'leave',
        loadChildren: './modules/leave/leave.module#LeaveModule'
      },
      {
        path: 'shiftment',
        loadChildren: './modules/shiftment/shiftment.module#ShiftmentModule'
      },
      {
        path: 'usergroup',
        loadChildren: './modules/usergroup/usergroup.module#UsergroupModule'
      },
      {
        path: 'menu',
        loadChildren: './modules/menu/menu.module#MenuModule'
      },
      {
        path: 'config',
        loadChildren: './modules/config/config.module#ConfigModule'
      },
      {
        path: 'activity-log',
        loadChildren:
          './modules/activity-log/activity-log.module#ActivityLogModule'
      },
      {
        path: 'attendance',
        loadChildren:
          './modules/attendance/attendance.module#AttendanceModule'
      },
      {
        path: 'permissionDenied',
        component: PermissionDeniedComponent
      },
      {
        path: 'auth/changepassword',
        component: ChangePasswordComponent
      }
    ]
  },
  {
    path: 'auth',
    component: AuthLayoutComponent,
    loadChildren: './modules/auth/auth.module#AuthModule'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
