import { Component, OnInit, Input, Output, EventEmitter, KeyValueDiffers } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  objectKeys = Object.keys;
  @Output() fncRemove: EventEmitter<any> = new EventEmitter();
  @Input() tableSvc: any;
  @Input() settings: object;
  columns: object;
  tools: object;
  differ: any;
  source = [];
  display = [];
  fullDesc: any;
  stringIsParams: boolean;

  constructor(
    private differs: KeyValueDiffers,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.differ = this.differs.find({}).create();
  }

  ngOnInit() {
    const c = this.settings['columns'];
    const tmp = {};
    for (const i in c) {
      if (typeof c[i].table === 'undefined' || c[i].table) {
        tmp[i] = c[i];
      }
    }
    this.columns = this.settings ? tmp : [];
    this.tools = this.settings ? this.settings['tools'] : {};
   // console.log('tmp', tmp);
   // console.log('this.columns', this.columns);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngDoCheck() {
    this.source = this.tableSvc.datas;
    const change = this.differ.diff(this);
    if (change) {
      change.forEachChangedItem(item => {
        // console.log('item changed', item);
        if (item.key === 'source') { this.dataMapping(); }
      });
    }
  }

  dataMapping() {
    // console.log('source', this.source);
    this.display = [];
    this.source.forEach(e => {
      const fDisplay = {};
      let dDisplay;
      // tslint:disable-next-line:forin
      for (const c in this.columns) {
       // console.log(typeof e[c]);
        if(e[c] !== null){
          dDisplay = (e[c]).toString();
          if (this.columns[c].type === 'date' && dDisplay) { dDisplay = (dDisplay !== '0000-00-00' || dDisplay !== '0000-00-00 00:00:00') ? this.formatDate(e[c], 'date') : ''; }
          if (this.columns[c].type === 'datetime' && dDisplay) { dDisplay = dDisplay !== '0000-00-00 00:00:00' ? this.formatDate(e[c], 'datetime') : ''; }
          if (this.columns[c].child && dDisplay) { dDisplay = e[c][this.columns[c].child.label]; }
          if (this.columns[c].as && dDisplay.toString()) { dDisplay = this.columns[c].as[e[c]]; }
          fDisplay[c] = dDisplay;
        //  console.log('==>', e[c], dDisplay)
        }
      }
      fDisplay[this.settings['primary']] = e[this.settings['primary']];
      this.display.push(fDisplay);
      // console.log('============')
    });
   // console.log('dataDisplay ==> ', this.display);
  }

  formatDate(d, t) {
    const date = new Date(d);
    const month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    // tslint:disable-next-line:max-line-length
    return this.addZero(date.getDate()) + ' ' + month_names_short[date.getMonth()] + ' ' + date.getFullYear() + `${t === 'datetime' ? ' ' + this.addZero(date.getHours()) + ':' + this.addZero(date.getMinutes()) : ''}`;
    // return this.addZero(date.getDate()) + ' ' + month_names_short[date.getMonth()] + ' ' + date.getFullYear() + ' ' + this.addZero(date.getHours()) + ':' + this.addZero(date.getMinutes()); -- backup
  }

  addZero(i) {
    if (i < 10) { i = '0' + i; }
    return i;
  }

  toPage(page, id) {
    console.log('toPage: ', page.replace('[id]', id), id);
    this.router.navigate([page.replace('[id]', id)]);
  }

  toForm(id = '') {
    this.router.navigate([`/${this.router.url.split('/')[1]}/form`, id]);
  }

  removeData(id): void { this.fncRemove.emit(id); }

  readDesc(str: string, content, col) {
    this.stringIsParams = col === 'params' ? true : false;
    this.fullDesc = col === 'params' ? JSON.parse(str) : str;
    this.modalService.open(content, { ariaLabelledBy: 'modal-read-desc' });
  }

}
