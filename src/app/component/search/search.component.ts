import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  objectKeys = Object.keys;
  @Input() settings: any;
  @Input() tableSvc: any;
  criteria = [];

  filterCriteria;
  key;

  constructor(private modalService: NgbModal, private dataSvc: DataService) {}

  ngOnInit() {
    this.criteria.push({ field: 'all', label: 'All' })
    for (let c in this.settings['columns']) {
      if (this.settings['columns'][c].search)
        this.criteria.push({
          field: c,
          label: this.settings['columns'][c].title
        });

        // add by yusup @ 16/04/2019 - buat type select yg optionnya dari table lain
        if (this.settings['columns'][c].type === 'select' && this.settings['columns'][c].options.length < 1) {
          this.loadSelect(c, this.settings['columns'][c]);
        }
    }
    this.filterCriteria = this.criteria[0].field
    console.log('settings on search', this.settings, this.criteria);
  }

  // add by yusup @ 16/04/2019 - buat type select yg optionnya dari table lain
  loadSelect(keyRoute, column) {
    console.log('loadSelect for:', keyRoute, column);
    let route = keyRoute
    if (keyRoute === 'department') { route = 'companyDepartment'; }
    this.dataSvc.get(`/${route}`, {where: JSON.stringify({deleted: '0'})})
      .subscribe(res => {
        console.log('loadSelect res:', res);
        const opt = [];
        res['data'].forEach(e => { opt.push({val: e[this.settings['columns'][keyRoute].child['label']], label: e[this.settings['columns'][keyRoute].child['label']]}); });
        this.settings['columns'][keyRoute].options = opt;
      });
  }

  submitFilter() {
    // if (['createdAt'].indexOf(this.filterCriteria) > -1 && this.key) {
    //   return this.tableSvc.filterDate(this.filterCriteria, this.key)
    // }
    console.log(this.filterCriteria, this.key)
    const {
      fieldTypeDate,
      fieldCollect,
      filterAllExcept,
      filterAllCollect
    } = this.settings;

    if (this.filterCriteria === 'all' && this.key) {
      console.log('A');
      const except = [...fieldCollect, ...filterAllExcept];
      this.tableSvc.filterAll(
        this.criteria,
        except,
        fieldTypeDate,
        this.key,
        filterAllCollect
      );
    } else if (fieldTypeDate.indexOf(this.filterCriteria) > -1 && this.key) {
      console.log('B');
      this.tableSvc.filterDate(this.filterCriteria, this.key);
    } else if (fieldCollect.indexOf(this.filterCriteria) > -1 && this.key) {
      console.log('C 1:', true);
      filterAllCollect.forEach(fa => {
        console.log('C 2:', fa);
        if (fa.fieldCollect === this.filterCriteria) {
          // console.log('C 3:', true);
          if (fa.model === 'department') { fa.model = 'companyDepartment'; }
          this.tableSvc
            .filterCollect(
              this.filterCriteria,
              this.key,
              fa.model,
              fa.fieldWhere,
              fa.fieldKey
            )
            .then(resultKey => {
              console.log('C 4:', resultKey);
              if (resultKey.length) {
                this.tableSvc.setWhere('or', resultKey, true);
              } else {
                this.tableSvc.setWhere(fa.fieldInParent, resultKey, true);
              }
            });
        }
      });
    } else {
      console.log('D');
      this.tableSvc.setWhere(this.filterCriteria, this.key, true);
    }
  }

  resetFilter() {
    this.tableSvc.where = {}
    this.key = ''
    this.tableSvc.requestData();
  }

  criteriaChange() {
    this.key = '';
  }

  open(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(result => {}, reason => {});
  }
}
