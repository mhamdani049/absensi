import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbTimepickerConfig, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as Moment from 'moment';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  providers: [NgbTimepickerConfig]
})
export class FormComponent implements OnInit {
  objectKeys = Object.keys;
  @Output() fncSubmit: EventEmitter<any> = new EventEmitter();
  @Input() source: any;
  @Input() settings: any;
  columnAll = {};
  columns = {};
  columnHide = {};
  time: NgbTimeStruct = {hour: 0, minute: 0, second: 0};

  formGroup: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private _location: Location,
    config: NgbTimepickerConfig
  ) {
    // customize default values of ratings used by this component tree
    config.seconds = true;
    config.spinners = false;
  }

  ngOnInit() {
    const c = this.settings['columns'];
    for (const i in c) {
      if (typeof c[i].form === 'undefined' || c[i].form) {
        this.columnAll[i] = c[i];
        c[i].type === 'hidden' ? this.columnHide[i] = c[i] : this.columns[i] = c[i];
      }
    }
    this.initForm();
  }

  initForm() {
    const colGroup = {};
    // tslint:disable-next-line:forin
    for (const c in this.columnAll) {
      colGroup[c] = [''];
    }
    for (const c in this.columns) {
      if (typeof this.columns[c].required === 'undefined' || this.columns[c].required) {
      colGroup[c].push(Validators.required);
      }
    }
    this.formGroup = this.formBuilder.group(colGroup);
    this.initValue();
  }

  initValue() {
    // tslint:disable-next-line:forin
    for (const c in this.columnAll) {
      if (typeof this.columns[c] !== 'undefined' && this.columns[c].type === 'date') {
        this.source[c] = {
          day: Number(Moment(this.source[c]).format('DD')),
          month: Number(Moment(this.source[c]).format('MM')),
          year: Number(Moment(this.source[c]).format('YYYY'))
        };
      }
      if (typeof this.columns[c] !== 'undefined' && this.columns[c].type === 'time') {
        if (this.source[c] !== undefined) {
          const getTime = this.source[c].split(':');
          this.time = {hour: Number(getTime[0]), minute: Number(getTime[1]), second: Number(getTime[2])};
        }
        this.source[c] = this.time;
      }
      this.formGroup.get(c).setValue(this.source[c]);
    }
  }

  get f() { return this.formGroup.controls; }

  onSubmit(): void {
    this.submitted = true;

    if (this.formGroup.invalid) {
      return;
    }

    this.fncSubmit.emit(this.formGroup.value);
  }

  backClicked() {
    this._location.back();
  }

}
