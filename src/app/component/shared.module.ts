import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule  } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgxJsonViewerModule } from 'ngx-json-viewer';

import { TableComponent } from '@component/table/table.component';
import { FormComponent } from '@component/form/form.component';
import { PaggingComponent } from '@component/pagging/pagging.component';
import { SearchComponent } from '@component/search/search.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    NgxPermissionsModule,
    NgxJsonViewerModule
  ],
  declarations: [
    TableComponent,
    SearchComponent,
    PaggingComponent,
    FormComponent
  ],
  exports: [
    TableComponent,
    SearchComponent,
    PaggingComponent,
    FormComponent
  ]
})
export class SharedModule { }
