import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxPermissionsService } from 'ngx-permissions';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { PageNotFoundComponent } from './layout/page-not-found/page-not-found.component';
import { FooterComponent } from './layout/footer/footer.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { NavComponent } from './layout/nav/nav.component';
import { AuthLayoutComponent } from './layout/auth-layout/auth-layout.component';
import { ContentLayoutComponent } from './layout/content-layout/content-layout.component';
import { PermissionDeniedComponent } from './layout/permission-denied/permission-denied.component';
import { ErrorInterceptorDialogComponent } from './layout/error-interceptor-dialog/error-interceptor-dialog.component';

import { NgxPermissionsModule } from 'ngx-permissions';
import { HttpAuthInterceptor } from './services/http.interceptor';
import { HttpErrorInterceptor } from './services/error.interceptor';
import { NotificationServices } from './services/notification.service';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { PermissionService } from '@app/services/permission.service';
import {
  CancelInterceptor,
  DEFAULT_TIMEOUT
} from '@app/services/cancel.interceptor'; // <============
import { ChangePasswordComponent } from './modules/auth/change-password/change-password.component';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { AttendanceModule } from './modules/attendance/attendance.module';

const configIO: SocketIoConfig = { url: 'http://dev-external.code.redwhite.co.id/', options: {path:'/sailsv1/api/socket.io/'} };

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    FooterComponent,
    NavComponent,
    SidebarComponent,
    NavbarComponent,
    AuthLayoutComponent,
    ContentLayoutComponent,
    PermissionDeniedComponent,
    ChangePasswordComponent,
    ErrorInterceptorDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    HttpClientModule,
    NgHttpLoaderModule.forRoot(), // <============ Don't forget to call 'forRoot()'!
    NgxPermissionsModule.forRoot(),
    SocketIoModule.forRoot(configIO),
    AttendanceModule
  ],
  entryComponents: [
    ErrorInterceptorDialogComponent
  ],
  providers: [
    PermissionService,
    {
      provide: APP_INITIALIZER,
      useFactory: (
        permissionService: PermissionService,
        ps: NgxPermissionsService
      ) =>
        function() {
          permissionService
            .load()
            .then(data => {
              // console.log('APP_INITIALIZER res:', data);
              return ps.loadPermissions(data);
            })
            .catch(err => console.log('APP_INITIALIZER err:', err));
        },
      deps: [PermissionService, NgxPermissionsService],
      multi: true
    },
    { provide: HTTP_INTERCEPTORS, useClass: HttpAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: CancelInterceptor, multi: true },
    { provide: DEFAULT_TIMEOUT, useValue: 30000 },
    NotificationServices
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
