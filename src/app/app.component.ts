import { Component, OnInit } from '@angular/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { HttpClient } from '@angular/common/http';

import { ApiResponse } from './api-response';
import {AuthService} from './services/auth.service';

import {  NotificationServices} from './services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-framework';
  notification: string;
  showNotification: boolean;


  constructor(
    private notificationService: NotificationServices,
    private permissionsService: NgxPermissionsService,
    private Auth: AuthService,
    private http: HttpClient) {}

    ngOnInit(): void {
      this.notificationService
            .notification$
            .subscribe(message => {
              this.notification = message;
              this.showNotification = true;
            });

      // backup @comment by yusup at 29 Jan 2018
      // if (this.Auth.isLoggedIn()) {
      //   // const perm = ["ADMIN", "EDITOR"];
      //   // this.permissionsService.loadPermissions(perm);
      //
      //   this.http.get<ApiResponse>('http://dev-external.code.redwhite.co.id/sailsv1/api/user/permission').subscribe((permissions) => {
      //       console.log('permissions', permissions.data.permission);
      //       // const perm = ["ADMIN", "EDITOR"]; example of permissions
      //       this.permissionsService.loadPermissions(permissions.data.permission);
      //   });
      //   }
    }

}
