import { Component } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { NgxPermissionsAllowStubDirective } from 'ngx-permissions';
import { HttpClientTestingModule } from '@angular/common/http/testing';
/*
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgHttpLoaderModule,
        HttpClientTestingModule
      ],

      declarations: [
        AppComponent

      ],
    }).compileComponents();
  }));

  it('should create the app', () => {

    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});*/


describe('AppComponent', () => {
  @Component({selector: 'app-comp',
      template: '<ng-template [ngxPermissionsExcept]="ADMIN"><div>123</div></ng-template>'})
  // tslint:disable-next-line:component-class-suffix
  class AppComp {
      data: any;
  }

  let comp;
  beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterTestingModule,
          NgHttpLoaderModule,
          HttpClientTestingModule
        ],
        declarations: [AppComponent, NgxPermissionsAllowStubDirective]});

  });

  it ('should create the app', () => {
   //   detectChanges(fixture);
   const fixture = TestBed.createComponent(AppComponent);
   comp = fixture.componentInstance;
    const app = fixture.debugElement.componentInstance;
      const content = fixture.debugElement.nativeElement.querySelector('div');

      expect(content).toBeTruthy();
      expect(content.innerHTML).toEqual('123');
  });
});

function detectChanges(fixture) {
  fixture.detectChanges();
}
