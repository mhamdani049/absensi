import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const tokenUser = localStorage.getItem('accessToken');
        if (tokenUser) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${tokenUser}`
                }
            });
        }

        return next.handle(request);
    }
}
