import {  Injectable,  Injector} from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable,  throwError} from 'rxjs';
import { catchError} from 'rxjs/operators';
import { ErrorInterceptorDialogService } from './error-interceptor-dialog.service'
import {  NotificationServices} from './notification.service';


@Injectable()


export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(
    private injector: Injector,
    public errorInterceptorDialogService: ErrorInterceptorDialogService
  ) {}

  intercept(request: HttpRequest < any > , next: HttpHandler): Observable < HttpEvent < any >> {
    // const notificationService = this.injector.get(NotificationServices);
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          // console.log("1/2. HttpErrorInterceptor >> ", error);
          if ([400].indexOf(error.status) < 0 ) {
            let reason = undefined;
            if (error.hasOwnProperty('error')) {
              let reason$ = error.error;
              if (reason$.hasOwnProperty('message')) reason = error;
              else reason = error.message;
            }
            else reason = error.message;
            // console.log("2/2. HttpErrorInterceptor:", reason, typeof reason);

            let data = {
              reason: typeof reason === 'string' ? reason : typeof reason.error.message === 'string' ? reason.error.message : reason.error.message.message ? reason.error.message.message : 'unknown',
              status: error.status ? error.status : error.name
            };
            this.errorInterceptorDialogService.open(JSON.stringify(data));
          }
          return throwError(error);

          // backup punya om heddy @ muhamad yusup hamdani at 31 Jan 2019
          // let errMsg = '';
          // // Client Side Error
          // if (error.error instanceof ErrorEvent) {
          //   if (!navigator.onLine) {
          //     // No Internet connection
          //    // notificationService.notify('No Internet Connection');
          //   }
          //   errMsg = `Error: ${error.error.message}`;
          //
          // } else { // Server Side Error
          //   errMsg = `Error Code: ${error.status},  Message: ${error.message}`;
          // }
          // // notificationService.notify(errMsg);
          // // return an observable
          // return throwError(errMsg);
        })
      );
  }
}
