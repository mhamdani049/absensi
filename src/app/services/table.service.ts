import { Injectable } from '@angular/core'
import { DataService } from './data.service'
import { HttpEvent, HttpEventType } from '@angular/common/http'
import * as _ from 'lodash'
import * as moment from 'moment'

@Injectable({
  providedIn: 'root'
})
export class TableService {
  constructor(private dataSvc: DataService) { }
  reqTableService$
  loading = false
  endPoint: string
  limit = 25
  datas = []
  metadata = {}
  starRow = 0
  endRow = 0
  numrows = 0
  page = 1
  totalPage = 1
  sort = ''
  sortBy = 'id'
  sortOrder = 'desc'
  collect = ''
  where = {}
  modelCriteriaMappingConfig = {}
  errorMessage = ''

  initialize(options: any = {}) {
    this.endPoint = options.endPoint || ''
    this.limit = options.limit || 25
    this.sort = options.sort || 'id desc'
    this.sortBy = this.sort.split(' ')[0] || 'id'
    this.sortOrder = this.sort.split(' ')[1] || 'desc'
    this.collect = options.collect || ''
    this.modelCriteriaMappingConfig = options.criteriaMapping || {}
  }

  setLimit(limit: number = 25) {
    this.limit = limit
    this.requestData()
  }

  setPage(page: number = 1) {
    if (page < 1 || page > this.totalPage) {
      return
    }
    this.page = page
    this.requestData()
  }

  setSort(sortBy: string, sortOrder: string) {
    let _sortOrder = sortOrder

    if (!_sortOrder) {
      if (sortBy === this.sortBy)
        _sortOrder = this.sortOrder === 'desc' ? 'asc' : 'desc'
      else _sortOrder = this.sortOrder
    }

    this.sortBy = sortBy
    this.sortOrder = _sortOrder

    this.sort = [this.sortBy, this.sortOrder].join(' ')
    this.requestData()
  }

  setWhere(fieldName: string = '', val: any = '', reload: boolean = false) {
    this.where = {}
    if (val === '') {
      delete this.where[fieldName]
    } else {
      this.where[fieldName] = val
    }

    this.page = 1

    // tslint:disable-next-line:no-unused-expression
    reload && this.requestData()
  }

  filterAll(
    criteria: any = [],
    except: any = [],
    fieldDate: any = [],
    key: any = '',
    fieldCollect: any = []
  ) {
    let _key = []
    const validFormat = [
      'DD/MM/YYYY',
      'DD-MM-YYYY',
      'DD MMM YYYY',
      'DD/MM/YYYY HH:mm',
      'DD-MM-YYYY HH:mm',
      'DD MMM YYYY HH:mm',
      'YYYY-MM-DD'
    ]
    criteria.forEach(c => {
      if (except.indexOf(c.field) < 0) {
        if (fieldDate.indexOf(c.field) > -1) {
          _key.push({ [c.field]: { contains: key } })

          // const validDate = moment(key, validFormat, true).isValid()
          const dateKey = moment(key, validFormat, true).format('YYYY-MM-DD')
          console.log('validDate', key, '==', dateKey)

          if (dateKey != 'Invalid date') {
            _key.push({ [c.field]: { contains: dateKey } })
            _key.push({ [c.field]: dateKey })
            const s = moment(key, validFormat)
              .startOf('day')
              .format('YYYY-MM-DD HH:mm')
            const e = moment(key, validFormat)
              .endOf('day')
              .format('YYYY-MM-DD HH:mm')
            _key.push({ [c.field]: { '>=': s, '<=': e } })
          }
        } else {
          _key.push({ [c.field]: { contains: key } })
        }
      }
    })

    if (!fieldCollect.length) {
      console.log('filterAll', _key)
      this.setWhere('or', _key, true)
    } else {
      console.log('fieldCollect.length', fieldCollect.length)
      var i = 0
      fieldCollect.forEach(fa => {
        this.filterCollect(
          fa.fieldCollect,
          key,
          fa.model,
          fa.fieldWhere,
          fa.fieldKey
        ).then((resultKey: any) => {
          i++
          console.log('resultKey', i, resultKey)
          resultKey.map(rk => _key.push(rk))
          if (i == fieldCollect.length) {
            console.log('filterAll', _key)
            this.setWhere('or', _key, true)
          }
        })
      })
    }
  }

  filterDate(criteria: any = '', key: any = '') {
    console.log(key)
    let _key = []
    if (key.year && key.month && key.day) {
      const keyDate = moment(
        `${key.year}-${key.month}-${key.day}`,
        'YYYY-M-D'
      ).format('YYYY-MM-DD')
      console.log(keyDate)
      _key.push({ [criteria]: { contains: keyDate } })
      _key.push({ [criteria]: keyDate })
      const s = moment(keyDate)
        .startOf('day')
        .format('YYYY-MM-DD HH:mm')
      const e = moment(keyDate)
        .endOf('day')
        .format('YYYY-MM-DD HH:mm')
      _key.push({ [criteria]: { '>=': s, '<=': e } })

      console.log('filterDate', _key)
      this.setWhere('or', _key, true)
    }
  }

  filterCollect(
    criteria: any = '',
    key: any = '',
    model: any = '',
    fieldWhere: any = '',
    fieldKey: any = ''
  ) {
    // console.log('table.service - filterCollect criteria:', criteria);
    // console.log('table.service - filterCollect key:', key);
    // console.log('table.service - filterCollect model:', model);
    // console.log('table.service - filterCollect fieldWhere:', fieldWhere);
    // console.log('table.service - filterCollect fieldKey:', fieldKey);

    let params = {
      limit: 50,
      where: JSON.stringify({ [fieldWhere]: { contains: key } })
    }

    return new Promise(resolve => {
      this.dataSvc.get(`/${model}`, params).subscribe(
        res => {
          console.log('res filterCollect', res)
          const { data } = res
          let thisKey = []
          if (data.length) {
            data.forEach(row => {
              thisKey.push({ [criteria]: row[fieldKey] })
            })
          }
          resolve(thisKey)
        },
        error => {
          console.error('error filterCollect', error)
          resolve([])
        }
      )
    })
  }

  modelWhereMappingToReqParamWhere() {
    const vm = this
    const criteria = {}

    _.each(Object.keys(vm.where), field => {
      if (field && vm.where) {
        if (vm.modelCriteriaMappingConfig[field]) {
          const oWhere = {}
          oWhere[vm.modelCriteriaMappingConfig[field]] = vm.where[field]
          criteria[field] = oWhere
        } else {
          criteria[field] = vm.where[field]
        }
      }
    })
    return criteria
  }

  requestData() {
    const vm = this
    vm.loading = true
    vm.datas = []
    vm.errorMessage = ''
    const reqParam = {}

    reqParam['limit'] = vm.limit
    reqParam['sort'] = vm.sort
    reqParam['skip'] = (vm.page - 1) * vm.limit

    if (vm.collect) {
      reqParam['collect'] = vm.collect
    }
    if (Array.isArray(vm.collect)) {
      if (vm.collect.length) {
        reqParam['collect'] = vm.collect.join(',')
      }
    }
    if (!_.isEmpty(vm.where)) {
      reqParam['where'] = JSON.stringify(vm.modelWhereMappingToReqParamWhere())
    }

    // backup takut masih dipake sama yang lain @ muhamad yusup hamdani at 30 Jan 2018
    // this.dataSvc
    //   .get(`${vm.endPoint}?`, reqParam)
    //   // .pipe(
    //   //   map(event => {
    //   //     console.log(this.getEventMessage(event))
    //   //   })
    //   // )
    //   .subscribe(
    //     res => {
    //       // console.log(res)
    //       const { data, metadata } = res;
    //       vm.datas = data;
    //       vm.metadata = metadata;
    //       vm.numrows = metadata.numrows;
    //       vm.starRow = metadata.skip + 1;
    //       vm.endRow = vm.page * vm.limit;
    //       vm.endRow = vm.numrows < vm.endRow ? vm.numrows : vm.endRow;
    //       vm.totalPage = Math.ceil(metadata.numrows / vm.limit);
    //       vm.loading = false;
    //     },
    //     error => {
    //       console.log('tableSvc error', error);
    //       vm.loading = false;
    //       vm.errorMessage = error.error
    //         ? error.error.message
    //         : 'Something Wrong';
    //     }
    //   );

    this.reqTableService$ = this.dataSvc
      .get(`${vm.endPoint}?`, reqParam)
      .subscribe(
        res => {
          const { data, metadata } = res
          vm.datas = data
          vm.metadata = metadata
          vm.numrows = metadata.numrows
          vm.starRow = metadata.skip + 1
          vm.endRow = vm.page * vm.limit
          vm.endRow = vm.numrows < vm.endRow ? vm.numrows : vm.endRow
          vm.totalPage = Math.ceil(metadata.numrows / vm.limit)
          vm.loading = false
        },
        error => {
          console.log('tableSvc error', error)
          vm.loading = false
          vm.errorMessage = error.error
            ? error.error.message
            : 'Something Wrong'
        }
      )
  }

  getEventMessage(event: HttpEvent<any>) {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        const percentDone = Math.round((100 * event.loaded) / event.total)
        return percentDone
      // return `File "${file.name}" is ${percentDone}% uploaded.`;

      case HttpEventType.Response:
        return `completed`

      default:
        return `event: ${event.type}.`
    }
  }
}
