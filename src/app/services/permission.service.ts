import { Injectable } from '@angular/core';

import { NgxPermissionsService } from 'ngx-permissions';
import { HttpClient } from '@angular/common/http';
import {Observable, of} from 'rxjs';
import { tap, map } from 'rxjs/operators';

import { ApiResponse } from '../api-response';
import { AuthService } from '@app/services/auth.service';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {
  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
    private permissionsService: NgxPermissionsService,
    private Auth: AuthService
  ) { }

  // backup @comment by yusup at 29 Jan 2018
  // initPermission() {
  //   this.http.get<ApiResponse>(`${this.apiUrl}/user/permission`).subscribe((permissions) => {
  //     this.permissionsService.loadPermissions(permissions.data.permission);
  //   });
  // }

  public load(): Promise<any> {
    if (this.Auth.isLoggedIn()) {
      return this.http.get<ApiResponse>( `${this.apiUrl}/user/permission`)
      .toPromise()
        .then(res => res.data.permission)
      .catch(this.handleError); } else { return Promise.reject('for access application you must login first.'); }
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  checkPermission(value: string) {
    return this.permissionsService.getPermission(value) === undefined ? false : true;
  }
}
