import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {


  public getAccessToken(): Observable < string > {
    const token: string = < string > localStorage.getItem('accessToken');
    return of(token);
  }


  public getRefreshToken(): Observable < string > {
    const token: string = < string > localStorage.getItem('refreshToken');
    return of(token);
  }


  public setAccessToken(token: string): StorageService {
    localStorage.setItem('accessToken', token);

    return this;
  }


  public setRefreshToken(token: string): StorageService {
    localStorage.setItem('refreshToken', token);

    return this;
  }


  public clear() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
  }


  public hasAccessToken() {
    const token = localStorage.getItem('accessToken');

    if (!token || token.length < 1) {
      return false;
    }

    return true;
  }

}
