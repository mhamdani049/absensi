import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import * as jwt_decode from 'jwt-decode';

import { StorageService } from './storage.service';
import { environment } from '@environments/environment';

interface AccessData {
  token: string;
  refreshToken: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiUrl = environment.apiUrl;
  loginState = false;
  constructor(private http: HttpClient, private tokenStorage: StorageService) { }

  login(identifier: string, password: string) {
    return this.http.post<any>(`${this.apiUrl}/user/token`, {
      identifier,
      password
    })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.loginState = true;
          this.saveAccessData(user);
        }
        return user;
      }));
  }
  loginWithGoogle(tokenGoogle: string) {
    return this.http.post<any>(`${this.apiUrl}/user/login-with-google`, {
      tokenGoogle
    })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.loginState = true;
          this.saveAccessData(user);
        }
        return user;
      }));
  }

  public getPayload() {
    return this.tokenStorage
      .getAccessToken()
      .pipe(map(token => jwt_decode(token)));
  }

  public isAuthorized(): Observable<boolean> {
    return this.tokenStorage
      .getAccessToken()
      .pipe(map(token => !!token));
  }

  public isLoggedIn(): boolean {
    if (this.loginState === true) {
      return true;
    }

    if (this.tokenStorage.hasAccessToken()) {
      return true;
    }

    return false;
  }


  public getAccessToken(): Observable<string> {
    return this.tokenStorage.getAccessToken();
  }

  logout() {
    // remove user from local storage to log user out
    this.tokenStorage.clear();
    localStorage.removeItem('currentUser');
  }

  private saveAccessData({ token, refreshToken }: AccessData) {
    this.tokenStorage
      .setAccessToken(token)
      .setRefreshToken(refreshToken);
  }
}
