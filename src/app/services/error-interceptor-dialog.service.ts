import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorInterceptorDialogComponent } from '@app/layout/error-interceptor-dialog/error-interceptor-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptorDialogService {

  constructor(private modalService: NgbModal) { }

  open(data) {
    // console.log("error-interceptor-dialog:", data);
    const modalRef = this.modalService.open(ErrorInterceptorDialogComponent, { backdrop  : 'static' });
    modalRef.componentInstance.data = JSON.parse(data);

    modalRef.result.then((result) => {
      console.log("Error Interceptor Dialog Service:", result);
    }).catch((error) => {
      console.log("Error Interceptor Dialog Service:", error);
    });
  }

}
