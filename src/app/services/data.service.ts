import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) { }
  apiUrl = environment.apiUrl;

  table() { }

  get(endPoint: string, params: any = {}) {
    return this.req('GET', endPoint, params);
  }

  put(endPoint: string, params: any = {}) {
    return this.req('PUT', endPoint, params);
    return this.http.put(`${this.apiUrl}${endPoint}`, params);
  }

  post(endPoint: string, params: any = {}) {
    // return this.req('POST', endPoint, params)
    return this.http.post(`${this.apiUrl}${endPoint}`, params);
  }

  private req(method: string, endPoint: string, params: any = {}) {

    const options = { reportProgress: true, params };
    return this.http.request<any>(method, `${this.apiUrl}${endPoint}`, options);

    // untuk demo test pending @ muhamad yusup hamdani at 30 Jan 2018
    // return this.http.request<any>(method, `http://10.234.99.220:8081/demo/1.post`)
  }
}
