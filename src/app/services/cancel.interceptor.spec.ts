import { TestBed } from '@angular/core/testing';
import { CancelInterceptor } from './cancel.interceptor';

describe('Cancel.Interceptor', () => {
  it('should create an instance', () => {
    const cancel: CancelInterceptor = TestBed.get(CancelInterceptor);
    expect(cancel).toBeTruthy();
  });
});
