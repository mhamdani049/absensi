import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PermissionService } from './permission.service';
import { NgxPermissionsRestrictStubDirective } from 'ngx-permissions';

describe('PermissionService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientTestingModule ],
    declarations: [
      NgxPermissionsRestrictStubDirective
    ],
  }));

  it('should be created', () => {
    const service: PermissionService = TestBed.get(PermissionService);
    expect(service).toBeTruthy();
  });
});
