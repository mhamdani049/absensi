import { TestBed } from '@angular/core/testing';

import { ErrorInterceptorDialogService } from './error-interceptor-dialog.service';

describe('ErrorInterceptorDialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ErrorInterceptorDialogService = TestBed.get(ErrorInterceptorDialogService);
    expect(service).toBeTruthy();
  });
});
