import {Component, Injectable, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router"

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '@services/auth.service';

@Component({
  selector: 'app-error-interceptor-dialog',
  templateUrl: './error-interceptor-dialog.component.html',
  styleUrls: ['./error-interceptor-dialog.component.css']
})
@Injectable({
  providedIn: "root"
})
export class ErrorInterceptorDialogComponent implements OnInit {
  @Input() data: any;

  constructor(public activeModal: NgbActiveModal, private router: Router, private authService: AuthService) { }
  ngOnInit() {
      // console.log('ErrorInterceptorDialogComponent loaded', this.data);
  }

  closeModal(msg) {
    this.activeModal.close(msg);
    if (this.data.status === 401) {
      this.authService.logout();
      location.reload(true);
    };
  }

}
