import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorInterceptorDialogComponent } from './error-interceptor-dialog.component';

describe('ErrorInterceptorDialogComponent', () => {
  let component: ErrorInterceptorDialogComponent;
  let fixture: ComponentFixture<ErrorInterceptorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorInterceptorDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorInterceptorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
