import { Component, OnInit } from '@angular/core';
import {AuthService} from '@services/auth.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {
  user: any;
  constructor (private Auth: AuthService) {}
    ngOnInit() {
      this.Auth.getPayload().subscribe(payload => {
        this.user = payload;
      });

    }
}
