import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor() { }

  navItems = [
    { link: '/dashboard', title: 'Dasobard' },
    { link: '/user', title: 'User' },
    { link: '/usergroup', title: 'User Group' },
    { link: '/auth/logout', title: 'Logout' }
  ];

  ngOnInit() {
  }

}
