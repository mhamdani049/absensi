import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
   {
        path: '/dashboard', title: 'Dashboard', icon: 'ft-home', class: 'nav-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: []
    },
    {
        path: '', title: 'Layouts', icon: 'ft-zap', class: 'nav-item has-sub', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false,
        submenu: [
            { path: '/1-column-layout', title: '1 column', icon: '', class: 'menu-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
            { path: '/2-columns-layout', title: '2 columns', icon: '', class: 'menu-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
            {
                path: '', title: 'Content Det. Sidebar', icon: '', class: 'menu-item has-sub', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false,
                submenu: [
                    { path: '/detached-left-sidebar-layout', title: 'Detached left sidebar', icon: '', class: 'menu-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
                    { path: '/detached-sticky-left-sidebar-layout', title: 'Detached sticky left sidebar', icon: '', class: 'menu-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
                    { path: '/detached-right-sidebar-layout', title: 'Detached right sidebar', icon: '', class: 'menu-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
                    { path: '/detached-sticky-right-sidebar-layout', title: 'Detached sticky right sidebar', icon: '', class: 'menu-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
                ]
            }
        ]
    },
    {
        path: '', title: 'Menu Levels', icon: 'ft-align-left', class: 'nav-item has-sub', badge: '5', badgeClass: 'badge badge badge-primary badge-pill float-right mr-2', isExternalLink: false, isNavHeader: false,
        submenu: [
            { path: 'javascript:;', title: 'Second Level', icon: '', class: 'menu-item', badge: '', badgeClass: '', isExternalLink: true, isNavHeader: false, submenu: [] },
            {
                path: '', title: 'Second Level Child', icon: '', class: 'menu-item has-sub', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false,
                submenu: [
                    { path: 'javascript:;', title: 'Third Level 1.1', icon: '', class: 'menu-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
                    { path: 'javascript:;', title: 'Third Level 1.2', icon: '', class: 'menu-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
                ]
            },
        ]
    },
    {
        path: '', title: 'Master', icon: 'icon-ellipsis ft-minus', class: 'navigation-header', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: true, submenu: []
    },
    { path: '/user', title: 'Users', icon: 'ft-life-buoy', class: 'nav-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
    { path: '/usergroup', title: 'User Group', icon: 'ft-folder', class: 'nav-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
    { path: '/menu', title: 'Menu Manager', icon: 'ft-align-left', class: 'nav-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },
    { path: '/activity-log', title: 'Activity Logs', icon: 'ft-activity', class: 'nav-item', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [] },


];
